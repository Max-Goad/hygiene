package com.maxgoad.hygiene.error;

public class UnexpectedStateException extends RuntimeException {

	private static final long serialVersionUID = -4448533631972388434L;

	public UnexpectedStateException(String message) {
		super(message);
	}
	
	public UnexpectedStateException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public UnexpectedStateException(Throwable cause) {
		super(cause);
	}
}
