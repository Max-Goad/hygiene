package com.maxgoad.hygiene.error;

public class FileAccessException extends RuntimeException {

	private static final long serialVersionUID = -1685685552446960093L;

	public FileAccessException(String message) {
		super(message);
	}
	
	public FileAccessException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public FileAccessException(Throwable cause) {
		super(cause);
	}
}
