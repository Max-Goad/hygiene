package com.maxgoad.hygiene.utils;

public class RegexConstants {	
	public static final String NO_FILTER = "";
	
	public static final String ACCESS_REGEX = "(public|private|protected)";
	public static final String MODIFIER_REGEX = "\\s*" + ACCESS_REGEX + "?+\\s*(static|abstract)?+\\s*(synchronized)?+\\s*(native)?+\\s*(final|volatile)?+\\s*(transient)?+\\s*+";
	public static final String CLASS_REGEX = "(?!return)[a-zA-Z_<>\\[\\]]++";
	public static final String NAME_REGEX = "\\s++(\\w++)";
	public static final String END_DECL_REGEX = "(;|\\s++=)";
	public static final String FN_VAR_REGEX = ".*\\((" + MODIFIER_REGEX + CLASS_REGEX + NAME_REGEX + ",?)+\\)";
	public static final String VARIABLE_REGEX = "^" + MODIFIER_REGEX + CLASS_REGEX + NAME_REGEX + END_DECL_REGEX + "|" + FN_VAR_REGEX;
	
	public static final String FN_HEADER_REGEX = "^" + MODIFIER_REGEX + CLASS_REGEX + NAME_REGEX + "\\(.*\\)";
	public static final String FN_CALL_REGEX = "\\w+\\(.*\\)\\s*;";
	
	public static final String CLASS_HEADER_REGEX = "^" + MODIFIER_REGEX + "(class|enum|interface)" + NAME_REGEX;
	
	public static final String COMMENT_REGEX = "(//|/\\*.*\\*/)";
	public static final String STD_COMMENT_REGEX = "^\\s*(//|/\\*)";
	
	public static final String EMPTY_ERROR_MSG_REGEX = "(\\w+Exception|StringIndexOutOfBounds)\\s*(\\((\")?\\s*(\")?(,\\s*\\w*)?\\))";
	
	public static final String SWITCH_REGEX = "^\\s*(switch)\\(.*\\)";
	
	public static final String TRY_HEADER_REGEX = "\\s*(try)\\s*\\{";
	public static final String TRY_CATCH_FINALLY_ONLY_FN = "\\{\\s*(try)\\s*\\{[^}]*\\}(\\s*(catch|finally)[^}]*\\{[^}]*\\})+\\s*\\}";
	
	public static final String BLOCK_STATEMENT_REGEX = "^\\s*(\\{\\s*)?((if|else\\s+if|while|for)\\s*\\(.*\\))|(do\\s*\\{)|(else\\s*)";
}
