package com.maxgoad.hygiene.analysis;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineMatch;

public class FileAnalyzer {
	
	JavaFileFilter fileFilter;
	Set<File> javaFiles;
	Set<GuidelineMatch> matchesFoundInFiles;

	
	public static FileAnalyzer CreateAnalyzerFor(File root) {
		return new FileAnalyzer(root);
	}
	
	private FileAnalyzer(File root) {
		fileFilter = new JavaFileFilter();
		javaFiles = fileFilter.getJavaFiles(root);
		matchesFoundInFiles = new HashSet<>();
	}
	
	public Set<GuidelineMatch> getMatchesFoundInFiles() {
		return matchesFoundInFiles;
	}
	
	public void runAnalysis(Set<Guideline> guidelines) {
		for (Guideline each : guidelines) {
			analyzeFilesUsing(each);
		}
	}
	
	private void analyzeFilesUsing(Guideline guideline) {
		if (javaFiles.isEmpty()) {
			throw new IllegalStateException("FileAnalyzer.analyzeFilesUsing found no java files with the root file/directory given!");
		}
		
		for (File each : javaFiles) {
			Set<GuidelineMatch> matches = new HashSet<>();
			try {
				matches = guideline.findPotentialProblems(each);
			} catch (Exception e) {
				System.out.println("Error attempting to analyze file " + each.getName() + " with Guideline " + guideline.getName());
				e.printStackTrace();
			}
			addMatchesToResultSet(matches);
		}
	}
	
	private void addMatchesToResultSet(Set<GuidelineMatch> matches) {
		matchesFoundInFiles.addAll(matches);
	}
}
