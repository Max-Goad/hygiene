package com.maxgoad.hygiene.analysis;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class JavaFileFilter {
	
	public Set<File> getJavaFiles(File inputFile) {
		if (inputFile == null)
			throw new IllegalArgumentException("Null argument in JavaFileFilter.filterJavaFiles");
		if (inputFile.isDirectory()) 
			return extractJavaFilesFromSubDirectories(inputFile);
		else
			return returnAsSetIfValid(inputFile);
	}

	private Set<File> extractJavaFilesFromSubDirectories(File directory) {
		if (!directory.isDirectory()) {
			throw new IllegalArgumentException("JavaFileFilter.extractJavaFilesFromDirectory was not given a directory in its parameters!");
		}
		
		Set<File> fileSet = new HashSet<>();
		for (File each : directory.listFiles()) {
			if (isJavaFile(each)) {
				fileSet.add(each);
			}
			if (each.isDirectory()) {
				Set<File> subDirectoryFiles = extractJavaFilesFromSubDirectories(each);
				fileSet.addAll(subDirectoryFiles);
			}
		}
		
		if (fileSet.isEmpty()) {
			throw new IllegalArgumentException("JavaFileFilter.extractJavaFilesFromDirectory found no java files in any of the given directories (and their children)!");
		}
		
		return fileSet;
	}
	
	private Set<File> returnAsSetIfValid(File inputFile) {
		if (!isJavaFile(inputFile)) {
			throw new IllegalArgumentException("JavaFileFilter.returnIfJavaFile found files claimed to be java files that were not!");
		}
		
		Set<File> fileSet = new HashSet<>();
		fileSet.add(inputFile);
		
		return fileSet;
	}
	
	private boolean isJavaFile(File inputFile) {
		String fileName = inputFile.getName();
		return fileName.endsWith(".java");
	}
}
