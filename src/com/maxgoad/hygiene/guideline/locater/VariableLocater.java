package com.maxgoad.hygiene.guideline.locater;

import static com.maxgoad.hygiene.utils.RegexConstants.*;

import java.io.File;
import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.guideline.codechunk.VariableFactory;

public class VariableLocater extends RegexLocater {

	protected VariableFactory factory = new VariableFactory();
	
	public VariableLocater() {
		super(VARIABLE_REGEX);
	}
	
	@Override
	public Set<RawCodeSection> findOccurrences(File javaFile) {
		this.occurrenceLocations = super.findOccurrences(javaFile);
		removeExtraOccurrences();
		return occurrenceLocations;
	}
	
	//Note: This shouldn't be necessary, but for some reason my regex above is not working
	//		as expected. When input into many different regex testers found online, it works
	//		perfectly as intended, but the possessive quantifiers simply do not work properly
	//		upon testing them in this environment. I've spent over 2 hours attempting to debug
	//		this and I don't believe it worth any more time, so instead I've done a workaround.
	//		If anyone reading this has a solution, feel free to implement it yourself.
	private void removeExtraOccurrences() {
		Set<RawCodeSection> fixedLocations = Sets.newSet();
		for (RawCodeSection each : occurrenceLocations) {
			try {
				factory.fromRawCodeSection(each);
				fixedLocations.add(each);
			} catch (IllegalArgumentException e) { 
				//System.out.println(e.getMessage());
			}
		}
		occurrenceLocations = fixedLocations;
	}
}
