package com.maxgoad.hygiene.guideline.locater;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.maxgoad.hygiene.error.FileAccessException;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;

public abstract class JavaOccurrenceLocater {
	
	protected List<String> fileLines;
	protected Set<RawCodeSection> occurrenceLocations;
	
	public abstract Set<RawCodeSection> findOccurrences(File javaFile);
	
	protected void resetMembers(File javaFile) {
		try {
			fileLines = Files.readAllLines(javaFile.toPath());
			occurrenceLocations = new TreeSet<>();
		} catch (IOException e) {
			throw new FileAccessException("JavaOccurrenceLocater.resetMembers");
		}
	}
}
