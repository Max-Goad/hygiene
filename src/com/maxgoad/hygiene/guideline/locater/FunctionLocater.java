package com.maxgoad.hygiene.guideline.locater;

import static com.maxgoad.hygiene.utils.RegexConstants.*;

import java.util.regex.Pattern;

import com.maxgoad.hygiene.error.UnexpectedStateException;

public class FunctionLocater extends AbstractScopeLocater {

	@Override
	protected Pattern getHeaderPattern() {
		return Pattern.compile(FN_HEADER_REGEX);
	}

	@Override
	protected void handleNestedBlock() {
		throw new UnexpectedStateException("FunctionLocater: Found a function header inside of another function header on line " + (this.currentLineNum + 1) + "! There is likely an error in the code provided.");
	}
	
}
