package com.maxgoad.hygiene.guideline.locater;

import java.io.File;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;

public class RegexLocater extends JavaOccurrenceLocater {

	protected Pattern patternToLocate;
	
	public RegexLocater(String regexToLocate) {
		this.patternToLocate = Pattern.compile(regexToLocate);
	}
	
	@Override
	public Set<RawCodeSection> findOccurrences(File javaFile) {
		resetMembers(javaFile);
		findOccurrencesInFileLines();
		return occurrenceLocations;
	}
	
	private void findOccurrencesInFileLines() {
		for(int i = 0; i < fileLines.size(); i++) {
			String currentLine = fileLines.get(i);
			Matcher matcher = patternToLocate.matcher(currentLine);
			if (matcher.find()) {
				occurrenceLocations.add(new RawCodeSection(i+1, currentLine));
			}
		}
	}

}
