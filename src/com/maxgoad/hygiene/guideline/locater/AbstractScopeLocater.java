package com.maxgoad.hygiene.guideline.locater;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.maxgoad.hygiene.error.UnexpectedStateException;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;

public abstract class AbstractScopeLocater extends JavaOccurrenceLocater {
	
	protected String currentLine;
	protected int currentLineNum;
	protected List<String> currentInstance;
	protected int currentInstanceStartLine;
	
	protected int braceCount;
	
	protected AbstractScopeLocater() {
		currentInstance = new ArrayList<String>();
		braceCount = 0;
	}
	
	protected abstract Pattern getHeaderPattern();
	protected abstract void handleNestedBlock();
	
	@Override
	public Set<RawCodeSection> findOccurrences(File javaFile) {
		resetMembers(javaFile);
		findAllInstancesInFile();
		return occurrenceLocations;
	}
	
	protected void findAllInstancesInFile() {
		for(currentLineNum = 0; currentLineNum < fileLines.size(); currentLineNum++) {
			currentLine = fileLines.get(currentLineNum);
			processLine();
		}
	}
	
	protected void processLine() {
		pushNewHeaders();
		if (!currentInstance.isEmpty()) {
			trackLine();
			incrementBraceCounts();
			processCompletedScopes();
		}
	}
	
	private void pushNewHeaders() {
		Pattern scopeHeaderPattern = getHeaderPattern();
		Matcher matcher = scopeHeaderPattern.matcher(currentLine);
		if (matcher.find()) {
			if (!currentInstance.isEmpty()) {
				handleNestedBlock();
			}
			else {
				currentInstance.add(currentLine);
				currentInstanceStartLine = currentLineNum;
			}
		}
	}
	
	private void trackLine() {
		if (braceCount > 0) {
			currentInstance.add(currentLine);
		}
	}
	
	private void incrementBraceCounts() {
		braceCount -= getExtraBraceCount();
		
		for (char each : currentLine.toCharArray()) {
			if (each == '{') {
				braceCount++;
			}
			else if (each == '}') {
				braceCount--;
				if (braceCount <= 0)
					return;
			}
		}
	}
	
	private int getExtraBraceCount() {
		int extraBraceCount = 0;
		Pattern extraBracePattern = Pattern.compile("\".*?\"");
		Matcher matcher = extraBracePattern.matcher(currentLine);
		while (matcher.find()) {
			String match = matcher.group();
			int openBraces = match.length() - match.replace("{", "").length();
			int closedBraces = match.length() - match.replace("}", "").length();
			extraBraceCount += (openBraces - closedBraces);
		}
		
		return extraBraceCount;
	}
	
	private void processCompletedScopes() {
		if (braceCount < 0) {
			throw new UnexpectedStateException("The brace counter has been decremented below 0. This means the Java file inputted must have an incorrect number of braces somewhere, and therefore is invalid!");
		}
		if (braceCount == 0) {
			occurrenceLocations.add(new RawCodeSection(currentInstanceStartLine+1, currentInstance));
			currentInstance = new ArrayList<String>();
		}
	}

}
