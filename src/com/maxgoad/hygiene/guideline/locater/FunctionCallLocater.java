package com.maxgoad.hygiene.guideline.locater;

import static com.maxgoad.hygiene.utils.RegexConstants.*;

import java.io.File;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;

public class FunctionCallLocater extends RegexLocater {

	public FunctionCallLocater() {
		super(FN_CALL_REGEX);
	}
	
	@Override
	public Set<RawCodeSection> findOccurrences(File javaFile) {
		this.occurrenceLocations = super.findOccurrences(javaFile);
		return occurrenceLocations;
	}
	
	public static String returnParams(String stringInQuestion) {
		Pattern markerPattern = Pattern.compile(FN_CALL_REGEX);
		Matcher matcher = markerPattern.matcher(stringInQuestion);
		if (matcher.find()) {
			return matcher.group(0);
		}
		else {
			return "";
		}
	}
}
