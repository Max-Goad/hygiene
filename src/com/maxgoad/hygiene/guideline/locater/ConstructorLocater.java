package com.maxgoad.hygiene.guideline.locater;

import static com.maxgoad.hygiene.utils.RegexConstants.*;

import java.io.File;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.maxgoad.hygiene.error.UnexpectedStateException;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;

public class ConstructorLocater extends AbstractScopeLocater {

	public static final String MISSING_CLASS_HEADER_ERROR_MESSAGE = "ConstructorLocater: Couldn't find a class name in file ";
	
	private String className;
	
	@Override
	protected Pattern getHeaderPattern() {
		return Pattern.compile("^\\s*" + ACCESS_REGEX + "?\\s+" + className + "+\\s*\\(.*\\)\\s*[{]?\\s*$");
	}
	
	@Override
	public Set<RawCodeSection> findOccurrences(File javaFile) {
		resetMembers(javaFile);
		className = getClassName(javaFile);
		findAllInstancesInFile();
		return occurrenceLocations;
	}

	@Override
	protected void handleNestedBlock() {
		throw new UnexpectedStateException("ConstructorLocater: Found a constructor header inside of another constructor header on line " + this.currentLineNum + "! There is likely an error in the code provided.");
	}
	
	private String getClassName(File file) {
		Pattern classHeaderPattern = Pattern.compile(CLASS_HEADER_REGEX);
		for (String eachLine : fileLines) {
			Matcher matcher = classHeaderPattern.matcher(eachLine);
			if (matcher.find()) {
				return matcher.group(8);
			}
		}
		throw new IllegalArgumentException(MISSING_CLASS_HEADER_ERROR_MESSAGE + file.getName());
	}

}
