package com.maxgoad.hygiene.guideline.locater;

import java.util.regex.Pattern;

import com.maxgoad.hygiene.utils.RegexConstants;

public class BlockStatementLocater extends AbstractScopeLocater {

	@Override
	protected Pattern getHeaderPattern() {
		return Pattern.compile(RegexConstants.BLOCK_STATEMENT_REGEX);
	}

	@Override
	protected void handleNestedBlock() {
	}

}
