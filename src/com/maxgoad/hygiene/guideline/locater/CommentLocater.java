package com.maxgoad.hygiene.guideline.locater;

import static com.maxgoad.hygiene.utils.RegexConstants.*;

import java.io.File;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;

public class CommentLocater extends RegexLocater {
	
	private SortedSet<RawCodeSection> lineComments;
	private SortedSet<RawCodeSection> finalComments;
	
	public CommentLocater() {
		super(COMMENT_REGEX);
	}
	
	@Override
	public Set<RawCodeSection> findOccurrences(File javaFile) {
		super.findOccurrences(javaFile);
		resetMembers();
		splitOccurrences();
		mergeLineComments();
		return finalComments;
	}
	
	private void resetMembers() {
		lineComments = new TreeSet<>();
		finalComments = new TreeSet<>();
	}

	private void splitOccurrences() {
		for (RawCodeSection each : occurrenceLocations) {
			String rawCode = "";
			for (String eachLine : each.getRawCode())
				rawCode += eachLine;
			
			if (rawCode.contains("//"))
				lineComments.add(each);
			else if (rawCode.contains("/*") && rawCode.contains("*/")) {
				finalComments.add(each);
			}
		}
	}
	
	private void mergeLineComments() {
		if (lineComments.size() < 2)
			return;
		
		SortedSet<RawCodeSection> newSet = new TreeSet<>();
		boolean noMoreMerges = false;
		while (!noMoreMerges) {
			
			newSet = attemptLineMerge();

			if (newSet.equals(lineComments)) {
				noMoreMerges = true;
			}
			else {
				lineComments = newSet;
				if (lineComments.size() == 1) {
					break;
				}
			}
		}
		
		finalComments.addAll(newSet);
	}
	
	private SortedSet<RawCodeSection> attemptLineMerge() {
		SortedSet<RawCodeSection> newSet = new TreeSet<>();
		
		Iterator<RawCodeSection> iterator = lineComments.iterator();
		RawCodeSection first = null;
		RawCodeSection second = iterator.next();
		while (iterator.hasNext()) {
			first = second;
			second = iterator.next();
			
			if (first.getEndLine() == second.getStartLine() - 1 
					&& isStandaloneComment(first.getCodeString())
					&& isStandaloneComment(second.getCodeString())) {
				RawCodeSection mergedSection = RawCodeSection.mergeCodeSections(first, second);
				newSet.add(mergedSection);
				
				if (iterator.hasNext())
					second = iterator.next();
				else
					break;
			}
			else {
				newSet.add(first);
			}
			
			if (!iterator.hasNext()) {
				newSet.add(second);
			}
		}
		
		return newSet;
	}
	
	private boolean isStandaloneComment(String line) {
		Pattern pattern = Pattern.compile(STD_COMMENT_REGEX);
		Matcher matcher = pattern.matcher(line);
		return matcher.find();
	}
}
