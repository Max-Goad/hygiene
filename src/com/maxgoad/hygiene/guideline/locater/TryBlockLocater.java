package com.maxgoad.hygiene.guideline.locater;

import java.util.regex.Pattern;

import com.maxgoad.hygiene.utils.RegexConstants;

public class TryBlockLocater extends AbstractScopeLocater {

	@Override
	protected Pattern getHeaderPattern() {
		return Pattern.compile(RegexConstants.TRY_HEADER_REGEX);
	}

	@Override
	protected void handleNestedBlock() {
	}

}
