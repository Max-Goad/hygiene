package com.maxgoad.hygiene.guideline;

import java.io.File;

import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;

public class GuidelineMatch {
	
	private Guideline guidelineUsed;
	private File fileAnalyzed;
	private RawCodeSection match;
	
	public GuidelineMatch(Guideline guidelineUsed, File fileAnalyzed, RawCodeSection match) {
		this.guidelineUsed = guidelineUsed;
		this.fileAnalyzed = fileAnalyzed;
		this.match = match;
	}

	public Guideline getGuidelineUsed() {
		return guidelineUsed;
	}
	
	public File getFileAnalyzed() {
		return fileAnalyzed;
	}
	
	public String getFileName() {
		return fileAnalyzed.getName();
	}
	
	public String getCodeMatch() {
		return match.getPrettyCodeString();
	}
	
	public int getStartLine() {
		return match.getStartLine();
	}
	
	public int getEndLine() {
		return match.getEndLine();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GuidelineMatch other = (GuidelineMatch) obj;
		if (fileAnalyzed == null) {
			if (other.fileAnalyzed != null)
				return false;
		} else if (!fileAnalyzed.equals(other.fileAnalyzed))
			return false;
		if (guidelineUsed == null) {
			if (other.guidelineUsed != null)
				return false;
		} else if (!guidelineUsed.equals(other.guidelineUsed))
			return false;
		if (match == null) {
			if (other.match != null)
				return false;
		} else if (!match.equals(other.match))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GuidelineMatch [guidelineUsed=" + guidelineUsed + ", fileAnalyzed=" + fileAnalyzed + ", match=" + match
				+ "]";
	}
}
