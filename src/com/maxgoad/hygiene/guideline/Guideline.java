package com.maxgoad.hygiene.guideline;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import com.maxgoad.hygiene.guideline.behavior.GuidelineBehavior;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;

public class Guideline {
	
	private GuidelineBehavior behavior;
		
	private Guideline() {
	}
	
	public static Guideline CreateNewGuideline(GuidelineType type) {
		Guideline newGuideline = new Guideline();
		newGuideline.setBehavior(type);
		return newGuideline;
	}
	
	public String getName() {
		return behavior.getName();
	}
	
	public String getGuidelineExplanation() {
		return behavior.getExplanation();
	}
	
	public Set<GuidelineMatch> findPotentialProblems(File fileToBeAnalyzed) {
		Set<RawCodeSection> codeMatches = behavior.findPotentialProblems(fileToBeAnalyzed);
		return convertToGuidelineMatches(fileToBeAnalyzed, codeMatches);
	}
	
	private void setBehavior(GuidelineType type) {
		this.behavior = GuidelineBehavior.createBehavior(type);
	}
	
	private Set<GuidelineMatch> convertToGuidelineMatches(File file, Set<RawCodeSection> codeSections) {
		Set<GuidelineMatch> result = new HashSet<>();
		for (RawCodeSection each : codeSections) {
			GuidelineMatch newMatch = new GuidelineMatch(this, file, each);
			result.add(newMatch);
		}
		return result;
	}
	
}
