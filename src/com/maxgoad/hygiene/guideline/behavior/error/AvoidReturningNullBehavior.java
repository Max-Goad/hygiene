package com.maxgoad.hygiene.guideline.behavior.error;


import com.maxgoad.hygiene.guideline.behavior.FilterWithRegexBehavior;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;
import com.maxgoad.hygiene.guideline.locater.RegexLocater;

public class AvoidReturningNullBehavior extends FilterWithRegexBehavior {

	public AvoidReturningNullBehavior() {
		invertFilter = true;
	}
	
	@Override
	public String getName() {
		return "Avoid Returning Null From Functions";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 110-111\n"
				+ "Any discussion that involves error handling should also discuss ways in which "
				+ "we easily invite errors. Returning null from a function creates work for ourselves, and "
				+ "throwing problems onto our callers. All it takes is one missing null check to send "
				+ "your application spinning out of control. It's easy to say a program that throws a "
				+ "NullPointerException is simply missing a null check, but the problem is that it already "
				+ "has too many! If you are tempted to return null from a function, consider either throwing an exception "
				+ "to be caught and handled in an appropriate place, or returning a special case object instead. If you are "
				+ "calling a null-returning method from a third-party API, consider wrapping it in an object that does one "
				+ "of those two things for you. Special case objects are often an easy remedy. Consider a function "
				+ "\"getEmployees()\", which returns a List of Employees. Instead of returning null if there are no "
				+ "employees, why not simply return an empty list? This will minimize the chance of a NullPointerException "
				+ "and your code will end up cleaner.";
	}

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new RegexLocater("(return)\\s+(null)\\s*;(?!\")");
	}

	@Override
	protected String getFilterRegex() {
		return "(\\/\\/|\\/\\*).*((return)\\s+(null)\\s*;)";
	}

}
