package com.maxgoad.hygiene.guideline.behavior.error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.maxgoad.hygiene.guideline.behavior.FilterWithRegexBehavior;
import com.maxgoad.hygiene.guideline.locater.FunctionCallLocater;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;

public class AvoidPassingNullBehavior extends FilterWithRegexBehavior {

	@Override
	public String getName() {
		return "Avoid Passing Null Values";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 111-112\n"
				+ "Returning null from methods is bad, but passing null into methods is worse. "
				+ "Unless you are working with an API which expects you to pass null, "
				+ "you should avoid pass null in your code whenever possible. "
				+ "If someone passes null as an argument, it is likely that a NullPointerException "
				+ "could be thrown at some point. You could either catch this and thrown a different "
				+ "exception, or perhaps put assertion in your functions, but either way you still "
				+ "have a runtime error! The rational approach is the forbid passing null by default "
				+ "in your code. When you do, you can code with the knowledge that a null in an arguement "
				+ "list is an indication of a problem, and end up with far fewer careless mistakes.";
	}
	
	@Override
	protected boolean containsMatch(String stringToMatch) {
		String functionCall = FunctionCallLocater.returnParams(stringToMatch);
		Pattern markerPattern = Pattern.compile(getFilterRegex());
		Matcher matcher = markerPattern.matcher(functionCall);
		return matcher.find();
	}

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new FunctionCallLocater();
	}

	@Override
	protected String getFilterRegex() {
		return "(?<!\")null(?!\\s*\")";
	}

}
