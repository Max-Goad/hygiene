package com.maxgoad.hygiene.guideline.behavior.error;

import static com.maxgoad.hygiene.utils.RegexConstants.*;

import com.maxgoad.hygiene.guideline.behavior.FilterWithRegexBehavior;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;
import com.maxgoad.hygiene.guideline.locater.RegexLocater;

public class AvoidNullErrorMessagesBehavior extends FilterWithRegexBehavior {

	@Override
	public String getName() {
		return "Avoid Exceptions Without Context Messages";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 107\n"
				+ "Each exception you throw should provide enough context to determine the source "
				+ "and location of an error. In Java, you can get the stack trace from any exception; "
				+ "however, a stack trace can't tell you the intent of the operation that failed! "
				+ "Create informative error messages and pass them along with your exceptions. Mention "
				+ "the operation that failed and the type of failure. If you are logging in your application, "
				+ "a good metric is to ensure you pass along enough information to be able to log "
				+ "your error entirely in the catch statement catching it.";
	}

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new RegexLocater(EMPTY_ERROR_MSG_REGEX);
	}

	@Override
	protected String getFilterRegex() {
		return NO_FILTER;
	}

}
