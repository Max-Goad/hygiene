package com.maxgoad.hygiene.guideline.behavior.error;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.behavior.FilterWithRegexBehavior;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.guideline.locater.FunctionLocater;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;
import com.maxgoad.hygiene.utils.RegexConstants;

public class ExtractErrorHandlingBehavior extends FilterWithRegexBehavior {

	public ExtractErrorHandlingBehavior() {
		invertFilter = true;
	}
	
	@Override
	public String getName() {
		return "Extract Error Handling Into Its Own Function";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 47\n"
				+ "Functions should do one thing. Error handling is one thing. "
				+ "Thus, a function that handles errors should do nothing else. "
				+ "This implies that if the keyword 'try' exists in a function, "
				+ "it should be the very first word in the function and that there "
				+ "should be nothing after the catch/finally blocks.";
	}
	
	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new FunctionLocater();
	}
	
	@Override
	protected void filterResults() {
		Set<RawCodeSection> rejectedOccurrences = Sets.newSet();
		for (RawCodeSection each : occurrences) {
			String codeString = each.getCodeString();
			if (containsTryBlock(codeString) && !containsMatch(codeString))
				rejectedOccurrences.add(each);
		}
		occurrences = rejectedOccurrences;
	}

	@Override
	protected String getFilterRegex() {
		return RegexConstants.TRY_CATCH_FINALLY_ONLY_FN;
	}
	
	private boolean containsTryBlock(String code) {
		Pattern tryBlockPattern = Pattern.compile(RegexConstants.TRY_HEADER_REGEX);
		Matcher matcher = tryBlockPattern.matcher(code);
		return matcher.find();
	}

}
