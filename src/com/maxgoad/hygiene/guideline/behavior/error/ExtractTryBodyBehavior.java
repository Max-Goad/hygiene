package com.maxgoad.hygiene.guideline.behavior.error;

import com.maxgoad.hygiene.guideline.behavior.FilterWithRegexBehavior;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;
import com.maxgoad.hygiene.guideline.locater.TryBlockLocater;

public class ExtractTryBodyBehavior extends FilterWithRegexBehavior {

	public ExtractTryBodyBehavior() {
		invertFilter = true;
	}
	
	@Override
	public String getName() {
		return "Extract Try Body Into Its Own Function";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 46-47\n"
				+ "Try/Catch blocks are ugly in their own right. They confuse the "
				+ "structure of the code and mix error processing with normal "
				+ "processing. So it is better to extract the bodies of the "
				+ "try/catch blocks out into functions of their own. This allows "
				+ "the error handling function to be easily understood and ignored. "
				+ "This provides a nice separation that makes the code easier to "
				+ "understand and modify.";
	}

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new TryBlockLocater();
	}
	
	@Override
	protected String getFilterRegex() {
		return "\\{\\s*([^};]+;)\\s*\\}";
	}

}
