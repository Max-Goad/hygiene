package com.maxgoad.hygiene.guideline.behavior;

import java.io.File;
import java.util.Set;

import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;

public abstract class SimpleBehavior extends GuidelineBehavior {

	protected Set<RawCodeSection> occurrences;
	
	protected abstract JavaOccurrenceLocater getLocater();
	protected abstract void filterResults();
	
	@Override
	public Set<RawCodeSection> findPotentialProblems(File fileToBeAnalyzed) {
		findGeneralOccurrences(fileToBeAnalyzed);
		filterResults();
		return occurrences;
	}
	
	protected void findGeneralOccurrences(File file) {			
		JavaOccurrenceLocater locater = getLocater();
		this.occurrences = locater.findOccurrences(file);
	}
	
	

}
