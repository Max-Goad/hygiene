package com.maxgoad.hygiene.guideline.behavior.formatting;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.maxgoad.hygiene.guideline.behavior.GuidelineBehavior;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.guideline.locater.RegexLocater;

public class KeepFileVerticalLinesSmallBehavior extends GuidelineBehavior {
	
	//See explanation below for reasoning behind this value
	private static final int LINE_MAXIMUM = 500;
	
	RawCodeSection fileHeader = null;
	
	@Override
	public String getName() {
		return "Keep Vertical Line Size Small";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 76-77 and 136-138\n"
				+ "How big should a source file be? In Java, file size is closely related to class size. "
				+ "The first rule of classes is that they should be small. The second rule of classes is "
				+ "that they should be smaller than that. No, we're not going to repeat the exact same "
				+ "text relating to functions. However, as with functions, the primary rule of classes relates to "
				+ "how small it is. With functions we measured size by counting physical lines. With classes, we use "
				+ "a different metric: Responsibilities! The name of the class should describe what responsibilities "
				+ "it fulfilles, and in fact is probably the first way of helping determine if your class will be too big! "
				+ "If you cannot derive a name for your class, it's likely too large. We should also be able to describe a "
				+ "class in about 25 words without using the words \"if\" \"or\" \"and\" or \"but\". "
				+ "At the end of the day, there is no hard or fast rule for file size in terms of lines per file, however "
				+ "it appears possible that you can build significant systems with files that are typically 200 lines long, "
				+ "with an upper limit of 500. This file size should be considered very desirable, as smaller files are easier "
				+ "to understand than larger ones.";
	}

	@Override
	public Set<RawCodeSection> findPotentialProblems(File fileToBeAnalyzed) {
		if (isFileTooLong(fileToBeAnalyzed)) {
			return getClassesInFile(fileToBeAnalyzed);
		} 
		else {
			return Collections.emptySet();
		}
	}
	
	private boolean isFileTooLong(File file) {
		try {
			List<String> lines = Files.readAllLines(file.toPath());
			return lines.size() >= LINE_MAXIMUM;
		} catch (IOException e) {
			return false;
		}
	}
	
	private Set<RawCodeSection> getClassesInFile(File file) {	
		RegexLocater locater = new RegexLocater("(public|protected|private)( static)?( abstract)? (class|enum|interface)[a-zA-Z ]*\\{");
		return locater.findOccurrences(file);
	} 

}
