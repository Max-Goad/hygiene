package com.maxgoad.hygiene.guideline.behavior.formatting;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.maxgoad.hygiene.guideline.behavior.GuidelineBehavior;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;

public class KeepFileHorizontalLinesSmallBehavior extends GuidelineBehavior {
	
	//See explanation below for reasoning behind this value
	private static final int LINE_SIZE_MAXIMUM = 120;
	
	private List<String> occurrences;
	
	@Override
	public String getName() {
		return "Keep Horizontal Line Size Small";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 85-86\n"
				+ "How wide should a line be? To answer that, I looked into how wide "
				+ "lines are in a set of typical programs. What I found was that the "
				+ "regularity of horizontal line size is impressive! Approximately "
				+ "30 percent of all lines are less than 10 characters, with an additional "
				+ "40 percent being between 20-60 characters. Programmers clearly prefer shorter lines. "
				+ "Hollerith suggested a limit of 80, which is a bit arbitrary, and I personally am not "
				+ "opposed to lines edging out to 100 or even 120. But beyond that is careless. "
				+ "I used to follow the rule that you should never have to scroll the the right, "
				+ "but monitors are too wide for that nowadays and younger programmers can shrink "
				+ "the font so small that they can fit 200 characters across the screen! Don't do that. "
				+ "I personally set my limit at 120 characters.";
	}

	@Override
	public Set<RawCodeSection> findPotentialProblems(File fileToBeAnalyzed) {
		readInLines(fileToBeAnalyzed);
		return getLongLinesAsCodeSections();
	}
	
	private void readInLines(File file) {
		try {
			occurrences = Files.readAllLines(file.toPath());
		} catch (IOException e) {
			occurrences = Collections.emptyList();
		}
	}
	
	private Set<RawCodeSection> getLongLinesAsCodeSections() {
		Set<RawCodeSection> sections = new HashSet<>();
		for (int i = 1; i <= occurrences.size(); i++) {
			String each = occurrences.get(i-1);
			if (each.trim().length() > LINE_SIZE_MAXIMUM) {
				sections.add(new RawCodeSection(i, each));
			}
		}
		return sections;
	}

}
