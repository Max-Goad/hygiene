package com.maxgoad.hygiene.guideline.behavior.comments;

public class AvoidPositionMarkersBehavior extends CommentBehavior {
	
	@Override
	public String getName() {
		return "Avoid Position Marker Comments";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 67\n"
				+ "Sometimes programmers like to mark a particular position in a source file. "
				+ "For example, I recently found this is a program I was looking through:\n"
				+ "// Actions ////////////////\n"
				+ "There are rare times when it makes sense to gather certain functions together under "
				+ "a banner such as this. But in general, they are clutter that should be eliminated. "
				+ "Again, functions and classes should be small and understandable, so if you find yourself "
				+ "needing banners to organize things, perhaps look into refactoring your code first! "
				+ "Think of it like this: A banner is startling and obvious if you don't see banners very often. "
				+ "So use them very sparingly, and only when the benefit is significant. Otherwise, they'll fall "
				+ "into background noise and be ignored, which defeats their whole purpose.";
	}

	@Override
	protected String getFilterRegex() {
		return "^\\s*(//|/\\*).*[/\\*`~!@#$%^&-_=+'\";:,\\\\]{3,}$";
	}

}
