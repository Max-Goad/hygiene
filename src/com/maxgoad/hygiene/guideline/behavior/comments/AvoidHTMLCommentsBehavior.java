package com.maxgoad.hygiene.guideline.behavior.comments;

public class AvoidHTMLCommentsBehavior extends CommentBehavior {
	
	@Override
	public String getName() {
		return "Avoid HTML Comments";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 69\n"
				+ "HTML in source code comments is an abomination. It makes comments hard "
				+ "to read in the one place where they should be easiest to read... the editor/IDE! "
				+ "If comments are going to be extracted by some tool (like Javadoc) to appear in a "
				+ "webpage, then it should be the responsibility of the tool, not the programmer, "
				+ "to adorn the comments with the appropriate HTML.";
	}

	@Override
	protected String getFilterRegex() {
		return "(<[/]?[a-zA-Z]+[/]?>)";
	}

}
