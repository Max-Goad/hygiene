package com.maxgoad.hygiene.guideline.behavior.comments;

import com.maxgoad.hygiene.guideline.behavior.FilterWithRegexBehavior;
import com.maxgoad.hygiene.guideline.locater.CommentLocater;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;

public abstract class CommentBehavior extends FilterWithRegexBehavior {
	
	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new CommentLocater();
	}
}
