package com.maxgoad.hygiene.guideline.behavior.comments;

public class AvoidClosingBraceMarkersBehavior extends CommentBehavior {
	
	@Override
	public String getName() {
		return "Avoid Closing Brace Markers";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 67-68\n"
				+ "Sometimes programmers will put special comments on closing braces... "
				+ "Although this might make sense for long functions with deeply nested structures, "
				+ "it serves only to clutter the kind of small and encapsulated functions that we prefer. "
				+ "So if you find yourself wanting to mark your closing braces, try to shorten your functions instead.";
	}

	@Override
	protected String getFilterRegex() {
		return "\\}\\s*(//|/\\*)";
	}

}
