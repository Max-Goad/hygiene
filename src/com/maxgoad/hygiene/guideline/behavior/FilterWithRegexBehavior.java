package com.maxgoad.hygiene.guideline.behavior;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.utils.RegexConstants;

public abstract class FilterWithRegexBehavior extends SimpleBehavior {

	protected abstract String getFilterRegex();
	protected boolean invertFilter = false;
	
	@Override
	protected void filterResults() {		
		if (getFilterRegex().equals(RegexConstants.NO_FILTER))
			return;

		Set<RawCodeSection> matchingOccurrences = Sets.newSet();
		Set<RawCodeSection> rejectedOccurrences = Sets.newSet();
		for (RawCodeSection each : occurrences) {
			String codeString = each.getCodeString();
			if (containsMatch(codeString))
				matchingOccurrences.add(each);
			else
				rejectedOccurrences.add(each);
		}
		if (invertFilter)
			occurrences = rejectedOccurrences;
		else
			occurrences = matchingOccurrences;
	}
	
	protected boolean containsMatch(String stringToMatch) {
		Pattern markerPattern = Pattern.compile(getFilterRegex());
		Matcher matcher = markerPattern.matcher(stringToMatch);
		return matcher.find();
	}
}
