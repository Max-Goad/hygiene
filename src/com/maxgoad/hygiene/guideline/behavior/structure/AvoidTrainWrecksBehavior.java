package com.maxgoad.hygiene.guideline.behavior.structure;

import java.io.File;
import java.util.Set;

import com.maxgoad.hygiene.guideline.behavior.GuidelineBehavior;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.guideline.locater.RegexLocater;

public class AvoidTrainWrecksBehavior extends GuidelineBehavior {

	private Set<RawCodeSection> occurrences;
	
	@Override
	public String getName() {
		return "Avoid Train Wrecks";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 97-99\n"
				+ "Code that is written with multiple levels of access on one line is known as "
				+ "a train wreck. This is because it looks like a bunch of coupled train cars. Chains "
				+ "of calls like this are generall considered to be sloppy style. It is usually best to "
				+ "split each call onto its own line. However, also consider that the need for multiple "
				+ "levels of calls might also be a sign that you're violating the Law of Demeter, which states "
				+ "that a module should not know about the innards of the object it manipulates. Consider "
				+ "writing a function that gets you exactly what you want in the first object, rather than having to make "
				+ "multiple descending calls on top of each other.";
	}

	@Override
	public Set<RawCodeSection> findPotentialProblems(File fileToBeAnalyzed) {
		findTrainWrecksInFile(fileToBeAnalyzed);
		return occurrences;
	}
	
	private void findTrainWrecksInFile(File file) {
		RegexLocater locater = new RegexLocater("(\\..*([(].*[)])){2,}");
		this.occurrences = locater.findOccurrences(file);
	}

}
