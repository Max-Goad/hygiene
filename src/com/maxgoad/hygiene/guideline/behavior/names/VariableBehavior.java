package com.maxgoad.hygiene.guideline.behavior.names;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.behavior.FilterWithRegexBehavior;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.guideline.codechunk.Variable;
import com.maxgoad.hygiene.guideline.codechunk.VariableFactory;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;
import com.maxgoad.hygiene.guideline.locater.VariableLocater;

public abstract class VariableBehavior extends FilterWithRegexBehavior {

	protected VariableFactory factory = new VariableFactory();
	
	@Override
	protected void filterResults() {
		Set<RawCodeSection> filteredOccurrences = Sets.newSet();
		for (RawCodeSection each : occurrences) {
			Set<Variable> varsInCode = factory.fromRawCodeSection(each);
			for (Variable eachVar : varsInCode) {
				if (containsMatch(eachVar.variableName))
					filteredOccurrences.add(each);
			}
		}
		occurrences = filteredOccurrences;
	}
	
	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new VariableLocater();
	}
}
