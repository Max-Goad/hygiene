package com.maxgoad.hygiene.guideline.behavior.names;

import com.maxgoad.hygiene.guideline.behavior.SimpleBehavior;
import com.maxgoad.hygiene.guideline.locater.ConstructorLocater;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;

public class AvoidOverloadedConstructorsBehavior extends SimpleBehavior {

	@Override
	public String getName() {
		return "Avoid Public Overloaded Constructors";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 25\n"
				+ "When constructors are overloaded, use static factory methods with names "
				+ "that describe the arguments. For example: \"Complex.FromRealNumber(23.0)\" "
				+ "is generally better than \"new Complex(23.0)\". This gives anyone using these "
				+ "classes the advantage of understanding what the arguments are actually being "
				+ "used for, and therefore help them better choose the appropriate type of "
				+ "construction. You can enforce these static factory methods by making the "
				+ "corresponding constructors private.";
	}

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new ConstructorLocater();
	}

	@Override
	protected void filterResults() {
		if (occurrences.size() <= 1) {
			occurrences.clear();
		}
	}

}
