package com.maxgoad.hygiene.guideline.behavior.names;

public class AvoidTypeEncodingsBehavior extends VariableBehavior {

	@Override
	public String getName() {
		return "Avoid Type Encodings In Names";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 23-24\n"
				+ "We have enough encodings to deal with without adding more to our burden. "
				+ "Encoding type or scope information into names simply adds an extra burden "
				+ "of deciphering. Encoded names are seldom pronounceable and are easy to mis-type. "
				+ "In days of old, we violated this rule out of necessity, and with regret. Older, "
				+ "more name-length-challenged languages forced or encouraged encodings through a "
				+ "lack of type checking (amongst other problems). In modern languages, we have much richer "
				+ "type systems, and the compilers remember and enforce the types. In Java, Objects are strongly "
				+ "types and editing environments have advanced such that they detect a type error long before you "
				+ "can run a compile! So nowadays, encodings are simply impediments. They make it harder to read "
				+ "the code, and they create the possibility that the name will mislead the reader as to the type.";
	}

	@Override
	protected String getFilterRegex() {
		return createComplexRegex();
	}
	
	private String createComplexRegex() {
		String[] types = getCaseInsensitiveTypes();
		String startRegex = createStartRegex(types);
		String endRegex = createEndRegex(types);
		return startRegex + "|" + endRegex;
	}
	
	private String[] getCaseInsensitiveTypes() {
		String[] types = { "bool", "boolean", "byte", "char", "character",
						   "short", "int", "integer", "long", "flt", "float",
						   "dbl", "double", "str", "string", "arr", "array"};
		String[] caseInsensitiveTypes = new String[types.length];
		for (int i = 0; i < types.length; i++) {
			char leadingChar = types[i].charAt(0);
			caseInsensitiveTypes[i] = "[" + Character.toUpperCase(leadingChar) + Character.toLowerCase(leadingChar) + "]" + types[i].substring(1);
		}
		return caseInsensitiveTypes;
	}
	
	private String createStartRegex(String[] types) {
		String returnString = "^(";
		for (String type : types)
			returnString += type + "|";
		return returnString.substring(0, returnString.length()-1) + ")[A-Z]";
	}
	
	private String createEndRegex(String[] types) {
		String returnString = "[a-z](";
		for (String type : types)
			returnString += type + "|";
		return returnString.substring(0, returnString.length()-1) + ")$";
	}

}
