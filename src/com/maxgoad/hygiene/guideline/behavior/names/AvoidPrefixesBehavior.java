package com.maxgoad.hygiene.guideline.behavior.names;

public class AvoidPrefixesBehavior extends VariableBehavior {

	@Override
	public String getName() {
		return "Avoid Unnecessary Variable Prefixes";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 24\n"
				+ "You don't need to prefix member variables with 'm_' anymore. "
				+ "Your classes and functions should be small enough that you don't need them. "
				+ "And you should be using an editing environment that highlights or colourizes "
				+ "members to make them distinct. People quickly learn to ignore the prefix "
				+ "(or suffix) to see the meaningful part of the name. The more we read the code, "
				+ "the less we see the prefixes. Eventually the prefixes become unseen clutter.";
	}
	
	@Override
	protected String getFilterRegex() {
		return "^([b-z][A-Z]|[a-zA-Z]_[a-zA-Z]|_{1,3}[a-zA-Z])";
	}

}
