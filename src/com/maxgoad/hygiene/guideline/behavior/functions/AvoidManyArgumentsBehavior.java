package com.maxgoad.hygiene.guideline.behavior.functions;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.behavior.SimpleBehavior;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.guideline.locater.FunctionLocater;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;

public class AvoidManyArgumentsBehavior extends SimpleBehavior {

	@Override
	public String getName() {
		return "Avoid Too Many Arguments";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 40-42\n"
				+ "The ideal number of arguments for a function is zero. Next comes one (monadic), "
				+ "followed by two (dyadic). Three arguments (triadric) should be avoided whenever possible. "
				+ "More than three arguments requires very special justification... and then "
				+ "shouldn't be used anyways. Arguments are hard. They require a lot of conceptual power. "
				+ "Inside of an object, passing around a variable as an argument rather than making it an instance variable "
				+ "causes readers to have to interpret it each time they saw it. When you're reading the story "
				+ "told by a module, many times the argument is at a different level of abstraction than the "
				+ "function name, and forces you to know a detail that isn't particularly important to understanding "
				+ "the general idea of a function. Arguments also make testing much harder, due to argument combinatorics. "
				+ "In general, look to reduce arguments if they do not add, or take away, understanding of what the "
				+ "function is supposed to be doing. Excessive arguments cause us to ignore ones that aren't relevant, and "
				+ "we should never ignore any part of code. The parts we ignore are where the bugs will hide.";
	}

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new FunctionLocater();
	}

	@Override
	protected void filterResults() {
		Set<RawCodeSection> matchingOccurrences = Sets.newSet();
		for (RawCodeSection each : occurrences) {
			String functionHeader = each.getRawCode().get(0);
			if (hasTooManyArguments(functionHeader))
				matchingOccurrences.add(each);
		}
		occurrences = matchingOccurrences;
	}
	
	private boolean hasTooManyArguments(String functionHeader) {
		int openParen = functionHeader.indexOf('(');
		int closedParen = functionHeader.lastIndexOf(')');
		String argumentSubstring = functionHeader.substring(openParen, closedParen);
		return argumentSubstring.split(",").length > 2;
	}

}
