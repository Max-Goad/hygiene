package com.maxgoad.hygiene.guideline.behavior.functions;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.behavior.SimpleBehavior;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.guideline.locater.BlockStatementLocater;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;

public class KeepBlocksSmallBehavior extends SimpleBehavior {

	@Override
	public String getName() {
		return "Keep Blocks Small";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 35\n"
				+ "Keeping functions small implies that the blocks within those functions "
				+ "(such as those following if, else, while, or for statements) "
				+ "should be small as well. In fact, because those statements are "
				+ "rarely used on their own within a function, they should actually "
				+ "be only one line long... and probably a function call. Not only "
				+ "does this keep the enclosing function small, but it also adds "
				+ "documentary value because the function called within the block "
				+ "can have a nicely descriptive name. This also implies that functions "
				+ "should not be large enough to hold nested structures. THerefore, "
				+ "the indent level of the function should not be greater than one or "
				+ "two. This, of course, makes functions easier to read and understand.";
	}
	
	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new BlockStatementLocater();
	}

	@Override
	protected void filterResults() {
		Set<RawCodeSection> matchingOccurrences = Sets.newSet();
		for (RawCodeSection each : occurrences) {
			if (each.getRawCode().size() > 4) //Magic number derived from above with a bit of wiggle room
				matchingOccurrences.add(each);
		}
		occurrences = matchingOccurrences;
	}

}
