package com.maxgoad.hygiene.guideline.behavior.functions;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.behavior.SimpleBehavior;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.guideline.locater.FunctionLocater;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;

public class KeepFunctionsSmallBehavior extends SimpleBehavior {

	@Override
	public String getName() {
		return "Keep Functions Small";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 34\n"
				+ "The first rule of functions is that they should be small. "
				+ "The second rule of functions is that they should be smaller than"
				+ "that! This is not an assertion that I can justify. I can't provide "
				+ "any references to research that shows that very small functions are better. "
				+ "What I can tell you is that for nearly four decades I have written functions of "
				+ "all different sizes. I've written 3000 line abominations, scads of functions "
				+ "in the 100 to 300 range, and many that were 20 to 30 lines long. What this "
				+ "experience has taught me, through long trial and error, is that functions"
				+ "should be very small. How short should a function be? Every function should be "
				+ "transparently obvious, tell a story and lead to you the next in a compelling order. "
				+ "If you follow those rules, functions tend to be as small as 2 to 4 lines in the body at maximum! "
				+ "Aim for that, with no functions over 10 lines in the body. If you find this impossible, consider "
				+ "your overall class/system design and see if there isn't a way improving that could also help your function length.";
	}
	
	@Override
	protected void filterResults() {
		Set<RawCodeSection> matchingOccurrences = Sets.newSet();
		for (RawCodeSection each : occurrences) {
			if (each.getRawCode().size() > 11) { //For magic number explanation, see above!
				matchingOccurrences.add(each);
			}
		}
		occurrences = matchingOccurrences;
	}

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new FunctionLocater();
	}

}
