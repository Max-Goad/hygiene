package com.maxgoad.hygiene.guideline.behavior.functions;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.behavior.SimpleBehavior;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.guideline.locater.FunctionLocater;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;

public class KeepCommandQuerySeparateBehavior extends SimpleBehavior {

	@Override
	public String getName() {
		return "Keep Command and Query Functions Separate";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 45-46\n"
				+ "Functions should either do something or answer something, but not "
				+ "both. Either your function should change the state of an object, "
				+ "or it should return some information about that object. Doing both "
				+ "often leads to confusion. We could attempt to solve this by renaming "
				+ "the function, but the only valid names in that situation are "
				+ "excessively descriptive and don't help the readability of the function.";
	}

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new FunctionLocater();
	}

	@Override
	protected void filterResults() {
		Set<RawCodeSection> matchingOccurrences = Sets.newSet();
		for (RawCodeSection each : occurrences) {
			String functionHeader = each.getRawCode().get(0);
			if (returnArgIsBoolean(functionHeader) && containsArguments(functionHeader))
				matchingOccurrences.add(each);
		}
		occurrences = matchingOccurrences;
	}
	
	private boolean returnArgIsBoolean(String functionHeader) {
		String headerWithoutArguments = functionHeader.substring(0, functionHeader.indexOf('('));
		String[] modifiers = headerWithoutArguments.split("\\s+");
		String returnType = modifiers[modifiers.length - 2];
		return returnType.equalsIgnoreCase("boolean");
	}
	
	private boolean containsArguments(String functionHeader) {
		String argumentSubstring = getArgumentString(functionHeader);
		return !containsNoContent(argumentSubstring);
	}
	
	private String getArgumentString(String functionHeader) {
		int openParen = functionHeader.indexOf('(');
		int closedParen = functionHeader.lastIndexOf(')');
		return functionHeader.substring(openParen, closedParen + 1);
	}
	
	private boolean containsNoContent(String argumentString) {
		Pattern emptyPattern = Pattern.compile("\\(\\s*\\)");
		Matcher patternMatcher = emptyPattern.matcher(argumentString);
		return patternMatcher.find();
	}

}
