package com.maxgoad.hygiene.guideline.behavior.functions;

import static com.maxgoad.hygiene.utils.RegexConstants.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.maxgoad.hygiene.error.FileAccessException;
import com.maxgoad.hygiene.guideline.behavior.FilterWithRegexBehavior;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;
import com.maxgoad.hygiene.guideline.locater.RegexLocater;

public class AvoidSwitchStatementsBehavior extends FilterWithRegexBehavior {

	@Override
	public String getName() {
		return "Avoid Switch Statements";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 37-38\n"
				+ "It's hard to make a small switch statement. Even a switch statement "
				+ "with only two cases is larger than I'd like a single block or functions to be! "
				+ "It's also hard to make a switch statment that only does one thing. By their nature, "
				+ "switch statements always do N things. Unfortunately, we can't always avoid "
				+ "switch statements, but we can make sure that each statement is buried in a low-level "
				+ "class and is never repeated. We do this, of course, with polymorphism. "
				+ "My general rule for switch statements is that they can be tolerated if they appear "
				+ "only once, are used to create polymorphic objects, and are hidden behind an "
				+ "inheritance relationship so the rest of the system can't see them. This, by the way, "
				+ "also applies in the exact same manner to if/else chains that function almost identically "
				+ "to switch statements. Of course, every circumstance is unique, and there are times when "
				+ "I voilate one or more parts of this rule, but it is still a good rule to try and follow "
				+ "as much as possible.";
	}

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new RegexLocater(SWITCH_REGEX);
	}

	@Override
	protected String getFilterRegex() {
		return NO_FILTER;
	}

	@Override
	public Set<RawCodeSection> findPotentialProblems(File fileToBeAnalyzed) {
		super.findPotentialProblems(fileToBeAnalyzed);
		filterIfClassNotAbstract(fileToBeAnalyzed);
		return occurrences;
	}
	
	private void filterIfClassNotAbstract(File referenceFile) {
		String classHeader = findClassHeader(referenceFile);
		if (classHeader.contains("abstract")) {
			occurrences.clear();
		}
	}
	
	private String findClassHeader(File referenceFile) {
		Pattern classHeaderPattern = Pattern.compile(CLASS_HEADER_REGEX);
		for (String each : getFileLines(referenceFile)) {
			Matcher matcher = classHeaderPattern.matcher(each);
			if (matcher.find()) {
				return each;
			}
		}
		return "";
	}
	
	private List<String> getFileLines(File file) {
		try {
			return Files.readAllLines(file.toPath());
		} catch (IOException e) {
			throw new FileAccessException("AvoidSwitchStatementsBehavior.getFileLines");
		}
	}

}
