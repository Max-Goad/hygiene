package com.maxgoad.hygiene.guideline.behavior.functions;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.behavior.SimpleBehavior;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.guideline.locater.FunctionLocater;
import com.maxgoad.hygiene.guideline.locater.JavaOccurrenceLocater;

public class AvoidFlagArgumentsBehavior extends SimpleBehavior {

	@Override
	public String getName() {
		return "Avoid Flag Arguments";
	}

	@Override
	public String getExplanation() {
		return "Paraphrased from Robert C. Martin's \"Clean Code\", page 41\n"
				+ "Flag arguments are ugly. Passing a boolean into a function "
				+ "is a truly terrible practice. It immediately complicates the "
				+ "signature of the method, loudly proclaiming that this function "
				+ "does more than one thing. It does one thing if the flag is "
				+ "true and another if the flag is false! We should instead be "
				+ "splitting the function into two with descriptive names and "
				+ "calling those functions separately when individually needed.";
	}
	
	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new FunctionLocater();
	}

	@Override
	protected void filterResults() {
		Set<RawCodeSection> matchingOccurrences = Sets.newSet();
		for (RawCodeSection each : occurrences) {
			String functionHeader = each.getRawCode().get(0);
			if (containsFlagArgument(functionHeader))
				matchingOccurrences.add(each);
		}
		occurrences = matchingOccurrences;
	}
	
	private boolean containsFlagArgument(String functionHeader) {
		int openParen = functionHeader.indexOf('(');
		int closedParen = functionHeader.lastIndexOf(')');
		String argumentSubstring = functionHeader.substring(openParen, closedParen);
		for(String each : argumentSubstring.split(",")) {
			if (each.contains("boolean")) {
				return true;
			}
		}
		return false;
	}

}
