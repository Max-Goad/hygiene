package com.maxgoad.hygiene.guideline.behavior;

import java.io.File;
import java.util.Set;

import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.comments.*;
import com.maxgoad.hygiene.guideline.behavior.error.*;
import com.maxgoad.hygiene.guideline.behavior.formatting.*;
import com.maxgoad.hygiene.guideline.behavior.functions.*;
import com.maxgoad.hygiene.guideline.behavior.names.*;
import com.maxgoad.hygiene.guideline.behavior.structure.*;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;

public abstract class GuidelineBehavior {
	
	public static GuidelineBehavior createBehavior(GuidelineType type) {
		switch(type) {
		case TEST:
		case TEST2:
			return new TestBehavior();
		
		//Comments
		case AVOID_CLOSING_BRACE_MARKERS:
			return new AvoidClosingBraceMarkersBehavior();
		case AVOID_HTML_COMMENTS:
			return new AvoidHTMLCommentsBehavior();
		case AVOID_POSITION_MARKERS:
			return new AvoidPositionMarkersBehavior();
		
		//Error
		case AVOID_NULL_ERROR_MESSAGES:
			return new AvoidNullErrorMessagesBehavior();
		case AVOID_PASSING_NULL:
			return new AvoidPassingNullBehavior();
		case AVOID_RETURNING_NULL:
			return new AvoidReturningNullBehavior();
		case EXTRACT_ERROR_HANDLING:
			return new ExtractErrorHandlingBehavior();
		case EXTRACT_TRY_BODY:
			return new ExtractTryBodyBehavior();
			
		//Formatting
		case KEEP_FILE_HORIZONAL_LINES_SMALL:
			return new KeepFileHorizontalLinesSmallBehavior();
		case KEEP_FILE_VERTICAL_LINES_SMALL:
			return new KeepFileVerticalLinesSmallBehavior();
		
		//Functions
		case AVOID_FLAG_ARGUMENTS:
			return new AvoidFlagArgumentsBehavior();
		case AVOID_MANY_ARGUMENTS:		
			return new AvoidManyArgumentsBehavior();
		case AVOID_SWITCH_STATEMENTS:
			return new AvoidSwitchStatementsBehavior();
		case KEEP_BLOCKS_SMALL:
			return new KeepBlocksSmallBehavior();
		case KEEP_COMMAND_QUERY_SEPARATE:
			return new KeepCommandQuerySeparateBehavior();
		case KEEP_FUNCTIONS_SMALL:
			return new KeepFunctionsSmallBehavior();
			
		//Names
		case AVOID_OVERLOADED_CONSTRUCTORS:
			return new AvoidOverloadedConstructorsBehavior();
		case AVOID_PREFIXES:
			return new AvoidPrefixesBehavior();
		case AVOID_TYPE_ENCODINGS:
			return new AvoidTypeEncodingsBehavior();
			
		//Structure
		case AVOID_TRAIN_WRECKS:
			return new AvoidTrainWrecksBehavior();
		
		default:
			throw new IllegalArgumentException("GuidelineBehavior.createBehavior doesn't know how to handle the type " + type);
		}
	}
	
	public abstract String getName();
	public abstract String getExplanation();
	public abstract Set<RawCodeSection> findPotentialProblems(File fileToBeAnalyzed);
}
