package com.maxgoad.hygiene.guideline.codechunk;

public class Variable {
	
	public String className;
	public String variableName;
	public LinePair declarationLine;
	
	public Variable(String className, String variableName, int declarationLine) {
		this.className = className;
		this.variableName = variableName;
		this.declarationLine = LinePair.oneLinePair(declarationLine);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Variable other = (Variable) obj;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (declarationLine == null) {
			if (other.declarationLine != null)
				return false;
		} else if (!declarationLine.equals(other.declarationLine))
			return false;
		if (variableName == null) {
			if (other.variableName != null)
				return false;
		} else if (!variableName.equals(other.variableName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Variable [className=" + className + ", variableName=" + variableName + ", declarationLine="
				+ declarationLine + "]";
	}	
}
