package com.maxgoad.hygiene.guideline.codechunk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RawCodeSection implements Comparable<RawCodeSection> {

	List<String> rawCode;
	LinePair linePair;
	
	public static RawCodeSection mergeCodeSections(RawCodeSection first, RawCodeSection last) {
		List<String> mergedCode = new ArrayList<>();
		mergedCode.addAll(first.getRawCode());
		mergedCode.addAll(last.getRawCode());
		return new RawCodeSection(first.getStartLine(), mergedCode);
	}
	
	public RawCodeSection(int startLine, String rawLine) {
		this.rawCode = Arrays.asList(rawLine);
		this.linePair = new LinePair(startLine, startLine + rawCode.size() - 1);
	}
	
	public RawCodeSection(int startLine, List<String> rawCode) {
		this.rawCode = rawCode;
		this.linePair = new LinePair(startLine, startLine + rawCode.size() - 1);
	}
	
	public int getStartLine() {
		return linePair.start;
	}
	
	public int getEndLine() {
		return linePair.end;
	}
	
	public List<String> getRawCode() {
		return rawCode;
	}
	
	public String getCodeString() {
		String codeString = "";
		for (String each : rawCode) {
			codeString += each;
		}
		return codeString;
	}
	
	public String getPrettyCodeString() {
		int minWhiteSpace = getMinWhiteSpace();
		String codeString = "";
		for (String each : rawCode) {
			String finalEach = each.replaceAll("^\\s+", "");
			for (int i = 0; i < getLeadingWhiteSpace(each) - minWhiteSpace; i++) {
				finalEach = "\t" + finalEach;
			}
			codeString += finalEach;
			codeString += "\n";
		}
		return codeString;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RawCodeSection other = (RawCodeSection) obj;
		if (linePair == null) {
			if (other.linePair != null)
				return false;
		} else if (!linePair.equals(other.linePair))
			return false;
		if (rawCode == null) {
			if (other.rawCode != null)
				return false;
		} else if (!rawCode.equals(other.rawCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RawCodeSection [rawCode=" + rawCode + ", linePair=" + linePair + "]";
	}

	@Override
	public int compareTo(RawCodeSection other) {
		if (this.getStartLine() < other.getStartLine())
			return -1;
		else if (this.getStartLine() > other.getStartLine())
			return 1;
		else {
			if (this.getEndLine() < other.getEndLine())
				return -1;
			else if (this.getEndLine() > other.getEndLine())
				return 1;
			else
				return 0;
		}
	}
	
	private int getMinWhiteSpace() {
		int currentMin = Integer.MAX_VALUE;
		for (String each : rawCode)
			currentMin = Math.min(currentMin, getLeadingWhiteSpace(each));
		return currentMin == Integer.MAX_VALUE ? 0 : currentMin;
	}
	
	private int getLeadingWhiteSpace(String text) {
		Pattern leadingWhiteSpace = Pattern.compile("^\\s+");
		Matcher matcher = leadingWhiteSpace.matcher(text);
		return matcher.find() ? matcher.group().length() : 0;
	}
	
}
