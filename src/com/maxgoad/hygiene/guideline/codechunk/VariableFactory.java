package com.maxgoad.hygiene.guideline.codechunk;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mockito.internal.util.collections.Sets;

import static com.maxgoad.hygiene.utils.RegexConstants.*;

public class VariableFactory {
	
	public Set<Variable> fromRawCodeSection(RawCodeSection codeSection) {
		String declaration = removeTailAndTrim(codeSection.getCodeString());
		String finalDeclaration = removeFunctionContext(declaration);
		return createVariables(finalDeclaration, codeSection.getStartLine());
	}
	
	private String removeTailAndTrim(String input) {
		String head = input.split("(=|;)",2)[0];
		if (verifyString(head, MODIFIER_REGEX + "(\\s*"+ CLASS_REGEX +")?" + NAME_REGEX))
			return head;
		else
			throw new IllegalArgumentException("String \""+ head +"\" failed verification during removeTailAndTrim()");
	}
	
	private String removeFunctionContext(String input) {
		if (verifyString(input, FN_VAR_REGEX)) {
			return input.replaceAll(".*\\(|\\).*", "");
		}
		else {
			return input;
		}
	}
	
	private Set<Variable> createVariables(String input, int lineNumber) {
		Set<Variable> variables = Sets.newSet();
		String[] declarationStrings = input.split(",[ ]?");
		for (String each : declarationStrings) {
			variables.add(createVariableFromDeclaration(each, lineNumber));
		}
		return variables;
	}
	
	private Variable createVariableFromDeclaration(String declarationString, int lineNumber) {
		String[] names = getNames(declarationString);
		return new Variable(names[0], names[1], lineNumber);
	}
	
	private String[] getNames(String input) {
		String names = removeModifiers(input);
		if (verifyString(names, "^" + CLASS_REGEX + NAME_REGEX + "$"))
			return names.split("\\s++");
		else
			throw new IllegalArgumentException("String \""+ names +"\" failed verification during getNames()");
	}
	
	private String removeModifiers(String input) {
		String[] sections = input.split(" ");
		String resultString = "";
		for (String each : sections) {
			if (!verifyString(each, "public|private|protected|static|synchronized|native|final|volatile|transient")) {
				resultString += each + " ";
			}
		}
		return resultString.trim();
	}
	
	//Refactor note: This can probably be its own class/function in a helper class
	//				 Can be potentially very, very useful
	private boolean verifyString(String stringToVerify, String regex) {
		Pattern verificationPattern = Pattern.compile(regex);
		Matcher matcher = verificationPattern.matcher(stringToVerify);
		return matcher.find();
	}
}
