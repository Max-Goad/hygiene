package com.maxgoad.hygiene.guideline.codechunk;

public class LinePair {

    public final int start;
    public final int end;

    public static LinePair oneLinePair(int startAndEnd) {
    	return new LinePair(startAndEnd, startAndEnd);
    }
    
    public LinePair(int start, int end) {
        this.start = start;
        this.end = end;
    }

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LinePair other = (LinePair) obj;
		if (end != other.end)
			return false;
		if (start != other.start)
			return false;
		return true;
	}

	@Override
    public String toString() {
        return "(" + start + ", " + end + ')';
    }
}
