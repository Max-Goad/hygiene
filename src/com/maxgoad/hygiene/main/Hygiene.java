package com.maxgoad.hygiene.main;

import java.io.File;
import java.util.*;

import com.maxgoad.hygiene.analysis.FileAnalyzer;
import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineMatch;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.report.HygieneReport;

public class Hygiene {

	private FileAnalyzer analyzer;
	
	private Set<Guideline> guidelines;
	private Set<GuidelineMatch> allMatches;
	
	public Hygiene() {
		guidelines = new HashSet<>();
	}
	
	public HygieneReport produceReport(File inputFile, GuidelineType... guidelineTypes) {
		generateGuidelines(guidelineTypes);
		analyzeDirectoryOrFile(inputFile);
		return HygieneReport.createReportFromMatches(allMatches);
	}
	
	public HygieneReport produceReport(File inputFile, Collection<GuidelineType> guidelineTypes) {
		return produceReport(inputFile, guidelineTypes.toArray(new GuidelineType[0]));
	}
	
	private void generateGuidelines(GuidelineType... guidelineTypes) {
		guidelines.clear();
		for (GuidelineType currentType : guidelineTypes) {
			Guideline newGuideline = Guideline.CreateNewGuideline(currentType);
			guidelines.add(newGuideline);
		}
	}
	
	private void analyzeDirectoryOrFile(File inputFile) {
		analyzer = FileAnalyzer.CreateAnalyzerFor(inputFile);
		analyzer.runAnalysis(guidelines);
		allMatches = analyzer.getMatchesFoundInFiles();
	}
}
