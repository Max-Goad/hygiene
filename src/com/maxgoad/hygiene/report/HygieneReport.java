package com.maxgoad.hygiene.report;

import java.util.*;

import com.maxgoad.hygiene.guideline.GuidelineMatch;

public class HygieneReport implements Iterable<HygieneReportItem> {

	private Set<HygieneReportItem> itemSet;
	
	public static HygieneReport createBlankReport() {
		return new HygieneReport();
	}
	
	public static HygieneReport createReportFromMatches(Set<GuidelineMatch> matches) {
		HygieneReport finalReport = new HygieneReport();
		for (GuidelineMatch match : matches) {
			HygieneReportItem newItem = new HygieneReportItem(match.getFileName(), match.getCodeMatch(), match.getGuidelineUsed());
			finalReport.addItem(newItem);
		}
		return finalReport;
	}
	
	private HygieneReport() {
		itemSet = new HashSet<>();
	}
	
	public void addItem(HygieneReportItem newItem) {
		itemSet.add(newItem);
	}
	
	public List<HygieneReportItem> getItems() {
		List<HygieneReportItem> list = new ArrayList<>();
		for (HygieneReportItem each : itemSet)
			list.add(each);
		return list;
	}

	@Override
	public Iterator<HygieneReportItem> iterator() {
		return itemSet.iterator();
	}
	
	@Override
	public String toString() {
		return "------\nReport\n------\n"	+ itemSet;
	}

	public boolean isEmpty() {
		return itemSet.isEmpty();
	}
}
