package com.maxgoad.hygiene.report;

import com.maxgoad.hygiene.guideline.Guideline;

public class HygieneReportItem {
	
	public String fileName;
	public String codeSnippet;
	public String guidelineName;
	public String explanation;
	
	public HygieneReportItem(String fileName, String codeSnippet, Guideline guideline) {
		this.fileName = fileName;
		this.codeSnippet = codeSnippet;
		this.guidelineName = guideline.getName();
		this.explanation = guideline.getGuidelineExplanation();
	}
	
	@Override
	public String toString() {
		return "ReportItem:\n"
				+ " File Name      - " + fileName.trim() + "\n"
				+ " Code Snippet   - " + codeSnippet.trim() + "\n"
				+ " Guideline Name - " + guidelineName.trim() + "\n"
				+ " Explanation    - " + explanation.trim() + "\n";
	}
	
	
	
}
