package com.maxgoad.hygiene.app.utils;

import java.io.File;
import java.util.Collection;

import com.maxgoad.hygiene.guideline.GuidelineType;

public class MainMenuResponse {

	private File file;
	private Collection<GuidelineType> guidelineTypes;
	
	public MainMenuResponse(File fileAbsolutePath, Collection<GuidelineType> guidelineTypes) {
		this.file = fileAbsolutePath;
		this.guidelineTypes = guidelineTypes;
	}

	public File getFile() {
		return file;
	}

	public Collection<GuidelineType> getGuidelineTypes() {
		return guidelineTypes;
	}
}
