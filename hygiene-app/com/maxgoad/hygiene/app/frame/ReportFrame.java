package com.maxgoad.hygiene.app.frame;

import java.awt.*;
import java.awt.event.*;
import java.util.List;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import com.maxgoad.hygiene.report.HygieneReport;
import com.maxgoad.hygiene.report.HygieneReportItem;

public class ReportFrame extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5478433496666508401L;
	
	private static final int preferredWidth = 400;
	
	private MainMenuFrame parent;
	private List<HygieneReportItem> reportItems;
	private int currentItemNum;
	
	private JPanel mainPanel;
	
	private JScrollPane codePanel;
	private JTextArea codeText;
	
	private JPanel guidelinePanel;
	
	private JLabel guidelineName;
	private JScrollPane explanationPanel;
	private JTextArea explanationText;
	
	private JButton nextItemButton;
	private JLabel buttonLabel;
	private JButton prevItemButton;

	public ReportFrame(MainMenuFrame parent, HygieneReport report) {
		super("Report Frame");
		
		this.parent = parent;
		this.reportItems = report.getItems();
		this.currentItemNum = 0;
	}

	@Override
	public void actionPerformed(ActionEvent source) {
		Object sourceObject = source.getSource();
		if (sourceObject == nextItemButton) {
			if (currentItemNum >= 0) {
				prevItemButton.setEnabled(true);
			}
			if (currentItemNum >= reportItems.size()-2) {
				nextItemButton.setEnabled(false);
			}
			currentItemNum++;
			update();
		}
		else if (sourceObject == prevItemButton) {
			if (currentItemNum <= reportItems.size()-1) {
				nextItemButton.setEnabled(true);
			}
			if (currentItemNum <= 1) {
				prevItemButton.setEnabled(false);
			}
			currentItemNum--;
			update();
		}
	}
	
	private void update() {
		HygieneReportItem currentItem = reportItems.get(currentItemNum);
		
		//Code Panel
		codeText.setText(currentItem.codeSnippet);
		TitledBorder title = BorderFactory.createTitledBorder("Code Snippet - From: " + currentItem.fileName);
		codePanel.setBorder(title);
		
		//Guideline Name
		Font font = new Font("Serif", Font.BOLD, 32);
		FontMetrics fm = guidelineName.getFontMetrics(font);
		float width = fm.stringWidth(currentItem.guidelineName);
		Font newFont = font.deriveFont(((preferredWidth / width) * font.getSize()) - 1);
		guidelineName.setText(currentItem.guidelineName);
		guidelineName.setFont(newFont);
		
		//Guideline Explanation
		explanationText.setText(currentItem.explanation);
		
		//Button Labal
		buttonLabel.setText((currentItemNum+1) + "/" + reportItems.size());
		
		//Update Calls
		codeText.update(getGraphics());
		codePanel.update(getGraphics());
		guidelineName.update(getGraphics());
		explanationText.update(getGraphics());
		buttonLabel.update(getGraphics());
		
		repaint();
	}
	
	public void create() {		
		createMainPanel();

		this.add(mainPanel);
		update();
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				parent.setEnabled(false);
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				parent.setEnabled(true);
			}
		});
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setResizable(false);
		this.pack();
		this.validate();
		this.setVisible(true);
	}

	private void createMainPanel() {
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		mainPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		createCodePanel();
		createGuidelinePanel();
	}
	
	private void createCodePanel() {
		codeText = new JTextArea();
		codeText.setEditable(false);
		codeText.setTabSize(2);
		
		codePanel = new JScrollPane(codeText);
		codePanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		codePanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		codePanel.setPreferredSize(new Dimension(preferredWidth*2, preferredWidth));
		
		//Add to main panel
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.ipadx = 0;
		constraints.ipady = 0;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.gridwidth = 1;
		constraints.gridx = 0;
		constraints.gridy = 0;
		mainPanel.add(codePanel, constraints);
	}
	
	private void createGuidelinePanel() {
		guidelinePanel = new JPanel();
		guidelinePanel.setLayout(new GridBagLayout());
		guidelinePanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		guidelinePanel.setPreferredSize(new Dimension(preferredWidth, preferredWidth));
		
		createGuidelineName();
		createGuidelineExplanation();
		createButtons();

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.ipadx = 0;
		constraints.ipady = 0;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.gridwidth = 1;
		constraints.gridx = 1;
		constraints.gridy = 0;
		mainPanel.add(guidelinePanel, constraints);
	}
	
	private void createGuidelineName() {
		String name = reportItems.get(currentItemNum).guidelineName;
		guidelineName = new JLabel(name, SwingConstants.CENTER);

		Font font = new Font("Serif", Font.BOLD, 32);
		FontMetrics fm = guidelineName.getFontMetrics(font);
		float width = fm.stringWidth(name);
		Font newFont = font.deriveFont((preferredWidth / width) * font.getSize());
		
		
		guidelineName.setFont(newFont);
		
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.ipadx = 25;
		constraints.ipady = 25;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.gridwidth = 3;
		constraints.gridx = 0;
		constraints.gridy = 0;
		guidelinePanel.add(guidelineName, constraints);
	}
	
	private void createGuidelineExplanation() {
		explanationText = new JTextArea(reportItems.get(currentItemNum).explanation);
		explanationText.setEditable(false);
		explanationText.setLineWrap(true);
		explanationText.setWrapStyleWord(true);
		
		explanationPanel = new JScrollPane(explanationText);
		explanationPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		TitledBorder title = BorderFactory.createTitledBorder("Explanation");
		explanationPanel.setBorder(title);
		
		//Add to main panel
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.ipadx = 0;
		constraints.ipady = 0;
		constraints.weightx = 1;
		constraints.weighty = 8;
		constraints.gridwidth = 3;
		constraints.gridx = 0;
		constraints.gridy = 1;
		guidelinePanel.add(explanationPanel, constraints);
	}
	
	private void createButtons() {
		nextItemButton = new JButton("Next Item");
		nextItemButton.addActionListener(this);
		nextItemButton.setEnabled(currentItemNum < reportItems.size() - 1);
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.ipadx = 25;
		constraints.ipady = 25;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.gridx = 2;
		constraints.gridy = 2;
		guidelinePanel.add(nextItemButton, constraints);
		
		
		buttonLabel = new JLabel("", SwingConstants.CENTER);
		
		constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.ipadx = 25;
		constraints.ipady = 25;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.gridx = 1;
		constraints.gridy = 2;
		guidelinePanel.add(buttonLabel, constraints);
		
		
		prevItemButton = new JButton("Prev Item");
		prevItemButton.addActionListener(this);
		prevItemButton.setEnabled(false);
		
		constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.ipadx = 25;
		constraints.ipady = 25;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.gridx = 0;
		constraints.gridy = 2;
		guidelinePanel.add(prevItemButton, constraints);
	}

}
