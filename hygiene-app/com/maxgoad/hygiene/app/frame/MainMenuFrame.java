package com.maxgoad.hygiene.app.frame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.*;
import java.util.concurrent.Phaser;

import javax.swing.*;
import javax.swing.border.*;

import com.maxgoad.hygiene.app.utils.CheckBoxList;
import com.maxgoad.hygiene.app.utils.MainMenuResponse;
import com.maxgoad.hygiene.guideline.GuidelineType;

public class MainMenuFrame extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -662245759686642827L;
	
	public boolean closed = false;

	private final Phaser phaser = new Phaser(1);
	
	private JPanel mainPanel;
	private JLabel title;
	private JButton runButton;

	private JPanel filePanel;
	private JTextField filePath;
	private JButton fileButton;
	private JFileChooser fileChooser;

	private JPanel guidelinePanel;
	private Vector<JCheckBox> guidelineCheckBoxes;
	private CheckBoxList checkBoxList;
	private JButton selectAllButton;
	private JButton deselectAllButton;

	public MainMenuFrame() {
		super("Main Menu");
		
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		
		File temp = new File("X:\\Dropbox\\University\\4th Year\\Winter 2017\\COMP Honours\\hygiene\\src\\com\\maxgoad\\hygiene");
		fileChooser.setCurrentDirectory(temp);
	}

	@Override
	public void actionPerformed(ActionEvent source) {
		Object sourceObject = source.getSource();
		if (sourceObject == fileButton) {
			int returnValue = fileChooser.showOpenDialog(this);

			if (returnValue == JFileChooser.APPROVE_OPTION) {
				String path = fileChooser.getSelectedFile().getAbsolutePath();
				filePath.setText(path);
			}
			else {
			}
		}
		else if (sourceObject == selectAllButton) {
			for (JCheckBox each : guidelineCheckBoxes) {
				each.setSelected(true);
				this.repaint();
			}
		}
		else if (sourceObject == deselectAllButton) {
			for (JCheckBox each : guidelineCheckBoxes) {
				each.setSelected(false);
				this.repaint();
			}
		}
		else if (sourceObject == runButton) {
			phaser.arrive();
		}
	}

	public MainMenuResponse waitForResponse() throws InterruptedException {
		if (closed)	throw new InterruptedException();
		
		//Wait until 'Run' button pressed
		phaser.awaitAdvanceInterruptibly(phaser.getPhase());
		
		//Get Checkbox Values
		Set<GuidelineType> types = new HashSet<>();
		for (JCheckBox each : guidelineCheckBoxes) {
			if (each.isSelected()) {
				types.add(GuidelineType.valueOf(each.getText()));
			}
		}
		
		File newFile = new File(filePath.getText());
		
		if (closed)	throw new InterruptedException();
		return new MainMenuResponse(newFile, types);
	}
	
	public void create() {

		createMainPanel();

		this.add(mainPanel);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				closed = true;
				phaser.arrive();
			}
		});
		this.pack();
		this.setVisible(false);
	}

	private void createMainPanel() {
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		mainPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		createTitle();
		createRunButton();
		createFilePanel();
		createGuidelinePanel();
	}

	private void createTitle() {
		title = new JLabel("HYGIENE APPLICATION", SwingConstants.CENTER);
		title.setFont(new Font("Serif", Font.BOLD, 32));

		//Add to main panel
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.ipadx = 25;
		constraints.ipady = 25;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.gridwidth = 1;
		constraints.gridx = 0;
		constraints.gridy = 0;
		mainPanel.add(title, constraints);
	}
	
	private void createRunButton() {
		runButton = new JButton("Run Hygiene");
		runButton.addActionListener(this);

		//Add to main panel
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.ipadx = 25;
		constraints.ipady = 25;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.gridx = 0;
		constraints.gridy = 2;
		mainPanel.add(runButton, constraints);
	}
	
	private void createFilePanel() {
		TitledBorder title = BorderFactory.createTitledBorder("File/Directory to Analyze");
		filePanel = new JPanel();
		filePanel.setLayout(new GridBagLayout());
		filePanel.setBorder(title);		
		filePanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		//File Path
		//filePath = new JTextField("X:\\Dropbox\\University\\4th Year\\Winter 2017\\COMP Honours\\hygiene\\src\\com\\maxgoad\\hygiene\\main");
		filePath = new JTextField();

		GridBagConstraints a = new GridBagConstraints();
		a.fill = GridBagConstraints.HORIZONTAL;
		a.ipadx = 5;
		a.ipady = 5;
		a.weightx = 10;
		a.gridwidth = 1;
		a.gridx = 0;
		filePanel.add(filePath, a);

		//File Button
		fileButton = new JButton("Open");
		fileButton.addActionListener(this);

		GridBagConstraints b = new GridBagConstraints();
		b.ipadx = 0;
		b.ipady = 0;
		b.weightx = 1;
		b.gridwidth = 1;
		b.gridx = 1;
		filePanel.add(fileButton, b);

		//Add to main panel
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.ipadx = 25;
		constraints.ipady = 25;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.gridwidth = 1;
		constraints.gridx = 0;
		constraints.gridy = 1;
		mainPanel.add(filePanel, constraints);
	}

	private void createGuidelinePanel() {

		guidelineCheckBoxes = new Vector<>();
		for (GuidelineType each : GuidelineType.getValidGuidelineTypes()) {
			guidelineCheckBoxes.add(new JCheckBox(each.name(),true));
		}

		checkBoxList = new CheckBoxList();
		checkBoxList.setListData(guidelineCheckBoxes);

		TitledBorder title = BorderFactory.createTitledBorder("Guidelines");
		guidelinePanel = new JPanel();
		guidelinePanel.setLayout(new GridBagLayout());
		guidelinePanel.setBorder(title);		

		GridBagConstraints a = new GridBagConstraints();
		a.weightx = 1;
		a.weighty = 20;
		a.gridwidth = 2;
		a.gridheight = 1;
		a.gridx = 0;
		a.gridy = 0;
		guidelinePanel.add(checkBoxList, a);

		selectAllButton = new JButton("Select All");
		selectAllButton.addActionListener(this);
		deselectAllButton = new JButton("Deselect All");
		deselectAllButton.addActionListener(this);

		GridBagConstraints b = new GridBagConstraints();
		b.weightx = 1;
		b.weighty = 1;
		b.gridwidth = 1;
		b.gridheight = 1;
		b.gridx = 0;
		b.gridy = 1;
		guidelinePanel.add(selectAllButton, b);

		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.weighty = 1;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 1;
		c.gridy = 1;
		guidelinePanel.add(deselectAllButton, c);

		//Add to main panel
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.ipadx = 25;
		constraints.ipady = 25;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.gridwidth = 1;
		constraints.gridheight = 3;
		constraints.gridx = 1;
		constraints.gridy = 0;
		mainPanel.add(guidelinePanel, constraints);
	}
}
