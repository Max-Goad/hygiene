package com.maxgoad.hygiene.app;


import javax.swing.JOptionPane;

import com.maxgoad.hygiene.app.frame.*;
import com.maxgoad.hygiene.app.utils.MainMenuResponse;
import com.maxgoad.hygiene.main.Hygiene;
import com.maxgoad.hygiene.report.HygieneReport;

public class HygieneExampleApplication {

	Hygiene hygiene;
	MainMenuFrame mainMenuWindow;
	ReportFrame reportWindow;
	
	MainMenuResponse response;
	HygieneReport report;

	public HygieneExampleApplication() {
		this.hygiene = new Hygiene();
		this.mainMenuWindow = new MainMenuFrame();
	}
	
    public static void main(String[] args) {
    	HygieneExampleApplication app = new HygieneExampleApplication();
    	app.start();
    }
    
    public void start() {
    	
    	createMainWindow();
        
    	while (!mainMenuWindow.closed) {
    		try {
    			response = mainMenuWindow.waitForResponse();
    			report = hygiene.produceReport(response.getFile(), response.getGuidelineTypes());
        		
    			if (report.isEmpty()) {
    				JOptionPane.showMessageDialog(mainMenuWindow, "No matches found in code!");
    			}
    			else {
    				createReportWindowWith(report);
    			}
    		}
		    catch (InterruptedException e) {
		    	break;
		    }
    		catch (IllegalArgumentException e) {
    			JOptionPane.showMessageDialog(mainMenuWindow, "There was an error with the files/directories you selected!\nFull Error Message:\n" + e.getMessage(), "InputError", JOptionPane.ERROR_MESSAGE);
    		}
    	}
    }
    
    private void createMainWindow() {
    	javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	mainMenuWindow.create();
            	mainMenuWindow.setVisible(true);
            }
        });
    }
    
    private void createReportWindowWith(HygieneReport report) {
    	javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	reportWindow = new ReportFrame(mainMenuWindow, report);
            	reportWindow.create();
            }
        });
    }
    
    

}
