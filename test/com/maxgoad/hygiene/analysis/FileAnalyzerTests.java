package com.maxgoad.hygiene.analysis;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.hamcrest.core.StringContains;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;
import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineMatch;

public class FileAnalyzerTests {
	
	private FileAnalyzer testAnalyzer;
	
	@Rule 
	public ExpectedException thrown = ExpectedException.none();
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	@Test
	public void testConstructionWithNullRootFile() {
		//Create null temporary file
		File testFile = null;
		
		//Set up exception expectations
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage(StringContains.containsString("JavaFileFilter.filterJavaFiles"));
		
		//Attempt Construction
		testAnalyzer = FileAnalyzer.CreateAnalyzerFor(testFile);
		
		fail("IllegalArgumentException should have been thrown!");
	}
	
	@Test
	public void testConstructionWithIncorrectRootFile() {
		//Create temporary file
		File testFile = null;
		try {
			testFile = tempFolder.newFile("test");
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		//Set up exception expectations
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage(StringContains.containsString("JavaFileFilter.returnIfJavaFile"));
		
		//Attempt Construction
		testAnalyzer = FileAnalyzer.CreateAnalyzerFor(testFile);
		
		fail("IllegalArgumentException should have been thrown!");
	}
	
	@Test
	public void testConstructionWithNoJavaFiles() {
		//Create temporary file
		File testRoot = tempFolder.getRoot();
		try {
			tempFolder.newFile("testA");
			tempFolder.newFile("testB");
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		//Set up exception expectations
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage(StringContains.containsString("JavaFileFilter.extractJavaFilesFromDirectory"));
		
		//Attempt Construction
		testAnalyzer = FileAnalyzer.CreateAnalyzerFor(testRoot);
		
		fail("IllegalArgumentException should have been thrown!");
	}
	
	@Test
	public void testAnalysisWithSingleFile() {
		//Create temporary file
		File testFile = null;
		try {
			testFile = tempFolder.newFile("test.java");
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		//Create test guideline and guideline match
		Guideline mockGuideline = Mockito.mock(Guideline.class);
		GuidelineMatch testMatch = new GuidelineMatch(mockGuideline, testFile, null);
		Set<GuidelineMatch> testMatchSet = Sets.newSet(testMatch);
		Set<Guideline> testGuidelineSet = Sets.newSet(mockGuideline);
		
		//Mock interactions
		Mockito.when(mockGuideline.findPotentialProblems(testFile)).thenReturn(testMatchSet);
		
		//Construct analyzer with test folder
		testAnalyzer = FileAnalyzer.CreateAnalyzerFor(testFile.getParentFile());
		
		//Run analysis
		testAnalyzer.runAnalysis(testGuidelineSet);
		
		//Attempt to get results
		Set<GuidelineMatch> matches = testAnalyzer.getMatchesFoundInFiles();
		
		//Assertions and Verifications
		assertEquals(1, matches.size());
		assertEquals(testMatchSet, matches);
		Mockito.verify(mockGuideline).findPotentialProblems(testFile);
		Mockito.verifyNoMoreInteractions(mockGuideline);
		
	}
	
	@Test
	public void testAnalysisWithMultipleFiles() {
		//Create temporary files and folders
		File testRoot = tempFolder.getRoot(),
			 testSubDirectory = null,
			 testFileA = null,
			 testFileB = null,
			 testFileC = null;
		try {
			testSubDirectory = tempFolder.newFolder("testSubDirectory");
			testFileA = tempFolder.newFile("testFileA.java");
			testFileB = tempFolder.newFile("testFileB.java");
			testFileC = tempFolder.newFile(testSubDirectory.getName() + "\\testFileC.java");
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		//Create test guideline and guideline matchs
		Guideline mockGuideline = Mockito.mock(Guideline.class);
		GuidelineMatch testMatchA = new GuidelineMatch(mockGuideline, testFileA, null);
		GuidelineMatch testMatchB = new GuidelineMatch(mockGuideline, testFileB, null);
		GuidelineMatch testMatchC = new GuidelineMatch(mockGuideline, testFileC, null);
		Set<Guideline> testGuidelineSet = Sets.newSet(mockGuideline);
		
		//Mock interactions
		Mockito.when(mockGuideline.findPotentialProblems(testFileA)).thenReturn(Sets.newSet(testMatchA));
		Mockito.when(mockGuideline.findPotentialProblems(testFileB)).thenReturn(Sets.newSet(testMatchB));
		Mockito.when(mockGuideline.findPotentialProblems(testFileC)).thenReturn(Sets.newSet(testMatchC));
		
		//Construct analyzer with test folder
		testAnalyzer = FileAnalyzer.CreateAnalyzerFor(testRoot);
		
		//Run analysis
		testAnalyzer.runAnalysis(testGuidelineSet);
		
		//Attempt to get results
		Set<GuidelineMatch> matches = testAnalyzer.getMatchesFoundInFiles();
		
		//Assertions and Verifications
		assertEquals(3, matches.size());
		assertEquals(Sets.newSet(testMatchA, testMatchB, testMatchC), matches);
		Mockito.verify(mockGuideline).findPotentialProblems(testFileA);
		Mockito.verify(mockGuideline).findPotentialProblems(testFileB);
		Mockito.verify(mockGuideline).findPotentialProblems(testFileC);
		Mockito.verifyNoMoreInteractions(mockGuideline);
		
	}

}
