package com.maxgoad.hygiene.analysis;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.hamcrest.core.StringContains;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

public class JavaFileFilterTests {

	private JavaFileFilter testFilter = new JavaFileFilter();
	
	@Rule 
	public ExpectedException thrown = ExpectedException.none();
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	@Test
	public void testWithSingleCorrectFile() {
		//Create temporary file
		String expectedName = "test.java";
		File tempFile = null;
		try {
			tempFile = tempFolder.newFile(expectedName);
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		//Run filter
		Set<File> testSet = testFilter.getJavaFiles(tempFile);
		
		//Assertions
		assertEquals(1, testSet.size());
		assertTrue(testSet.contains(tempFile));
	}
	
	@Test
	public void testWithSingleIncorrectFile() {		
		//Create temporary file
		String expectedName = "test.txt";
		File tempFile = null;
		try {
			tempFile = tempFolder.newFile(expectedName);
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		//Set up exception expectations
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage(StringContains.containsString("JavaFileFilter.returnIfJavaFile"));
		
		//Run filter
		testFilter.getJavaFiles(tempFile);

		fail("IllegalArgumentException should have been thrown!");
	}
	
	@Test
	public void testWithNullParameter() {
		//Create temporary file
		File tempFile = null;

		//Set up exception expectations
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage(StringContains.containsString("JavaFileFilter.filterJavaFiles"));
		
		//Run filter
		testFilter.getJavaFiles(tempFile);
		
		fail("IllegalArgumentException should have been thrown!");
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testWithMultiLevelDirectoryStructureAndNoJavaFiles() {		
		//Create temporary directories
		File tempDirectoryRoot = tempFolder.getRoot();
		File tempSubDirectoryA = null;
		
		try {
			tempSubDirectoryA = tempFolder.newFolder("testFolderA");
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		//Create temporary files
		File tempFileRootTxt, tempFileA1 = null, tempFileA2 = null;
		
		try {
			tempFileRootTxt = tempFolder.newFile("\\testFileRoot.txt");
			tempFileA1 = tempFolder.newFile(tempSubDirectoryA.getName() + "\\testFile1.exe");
			tempFileA2 = tempFolder.newFile(tempSubDirectoryA.getName() + "\\testFile2.other");
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		
		/*	Directory Structure:
		 *  ====================
		 * 	ROOT
		 *  -testFileRoot.txt
		 *  -testFolderA
		 *  --testFile1.exe
		 *  --testFile2.other
		 *  ====================
		 */

		//Set up exception expectations
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage(StringContains.containsString("JavaFileFilter.extractJavaFilesFromDirectory"));
		
		//Run filter
		Set<File> testSet = testFilter.getJavaFiles(tempDirectoryRoot);
		
		fail("IllegalArgumentException should have been thrown!");
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testWithMultiLevelDirectoryStructure() {		
		//Create temporary directories
		File tempDirectoryRoot = tempFolder.getRoot();
		File tempSubDirectoryA = null, tempSubDirectoryB = null, tempSubDirectoryC = null;
		
		try {
			tempSubDirectoryA = tempFolder.newFolder("testFolderA");
			tempSubDirectoryB = tempFolder.newFolder("testFolderB");
			tempSubDirectoryC = tempFolder.newFolder(tempSubDirectoryB.getName(), "testFolderC");
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		//Create temporary files
		File tempFileRootJava = null, tempFileRootTxt, 
			 tempFileA1 = null, tempFileA2 = null, tempFileA3,
			 tempFileC1 = null;
		
		try {
			tempFileRootJava = tempFolder.newFile("\\testFileRoot.java");
			tempFileRootTxt = tempFolder.newFile("\\testFileRoot.txt");
			tempFileA1 = tempFolder.newFile(tempSubDirectoryA.getName() + "\\testFile1.java");
			tempFileA2 = tempFolder.newFile(tempSubDirectoryA.getName() + "\\testFile2.java");
			tempFileA3 = tempFolder.newFile(tempSubDirectoryA.getName() + "\\testFile3.txt");
			tempFileC1 = tempFolder.newFile(tempSubDirectoryB.getName() + "\\" +
											tempSubDirectoryC.getName() + "\\testFile1.java");
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		
		/*	Directory Structure:
		 *  ====================
		 * 	ROOT
		 * 	-testFileRoot.java	<--
		 *  -testFileRoot.txt
		 *  -testFolderA
		 *  --testFile1.java	<--
		 *  --testFile2.java	<--
		 *  --testFile3.txt
		 *  -testFolderB
		 *  --testFolderC
		 *  ---testFile1.java	<--
		 *  ====================
		 */

		//Run filter
		Set<File> testSet = testFilter.getJavaFiles(tempDirectoryRoot);
		
		//Assertions
		assertEquals(4, testSet.size());
		assertTrue(testSet.contains(tempFileRootJava));
		assertTrue(testSet.contains(tempFileA1));
		assertTrue(testSet.contains(tempFileA2));
		assertTrue(testSet.contains(tempFileC1));
	}

}
