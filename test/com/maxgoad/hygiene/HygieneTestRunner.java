package com.maxgoad.hygiene;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class HygieneTestRunner {
	
	private static Result result = null;
	
	public static void main(String[] args) {
		runTests();
		printResult();
		printAllFailureMessages();
	}
	
	private static void runTests() {
		result = JUnitCore.runClasses(HygieneTestSuite.class);
	}
	
	private static void printResult() {
		if (result.wasSuccessful())
			System.out.println("All tests succeeded!");
		else
			System.out.println("Some tests have failed. See below for full list of failures/reasons (all others passed) \n");
	}
	
	private static void printAllFailureMessages() {
		for (Failure failure : result.getFailures()) {
			String failureString = failure.getMessage();
			if (failureString != null)
				printFailure(failure);
		}
	}
	
	private static void printFailure(Failure failure) {
		String output = "Class: 	" + getTestClass(failure.getTestHeader()) + "\n"
				      + "Test: 	" + getTestName(failure.getTestHeader()) + "\n"
				      + "Reason: " + failure.getMessage() + "\n";
		System.out.println(output);
	}
	
	private static String getTestClass(String fullString) {
		int startIndex = fullString.lastIndexOf(".");
		int endIndex   = fullString.indexOf(")");
		return fullString.substring(startIndex+1, endIndex);
	}
	
	private static String getTestName(String fullString) {
		return fullString.substring(0, fullString.indexOf("("));
	}
}
