package com.maxgoad.hygiene.utils;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.util.*;

import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;

public class TestUtils {
	
	public static <T> void assertEqualSets(Set<T> expectedSet, Set<T> actualSet) {
		
		HashMap<T, Boolean> expectedTracker = new HashMap<>();
		HashMap<T, Boolean> actualTracker = new HashMap<>();
		
		for (T eachExpected : expectedSet) {
			expectedTracker.put(eachExpected, false);
		}
		for (T eachActual : actualSet) {
			actualTracker.put(eachActual, false);
		}
		
		for (T eachExpected : expectedSet) {
			for (T eachActual : actualSet) {
				if (eachExpected.equals(eachActual)) {
					expectedTracker.put(eachExpected, true);
					actualTracker.put(eachActual, true);
				}
			}
		}
		
		boolean fail = false;
		
		for (T eachExpected : expectedSet) {
			if (expectedTracker.get(eachExpected) == false) {
				System.out.println(eachExpected + " didn't find a match in the actual!");
				fail = true;
			}
		}
		for (T eachActual : actualSet) {
			if (actualTracker.get(eachActual) == false) {
				System.out.println(eachActual + " didn't find a match in the expected!");
				fail = true;
			}
		}
		if (fail) {
			fail("Set elements do not match!");
		}
	}
	
	public static RawCodeSection getCodeSection(File file, int startLine, int endLine) throws IOException {
		List<String> allLines = Files.readAllLines(file.toPath());
		List<String> returnedLines = new ArrayList<>();
		
		for (int i = 1; i <= endLine; i++) {
			if (i >= startLine) {
				try {
					returnedLines.add(allLines.get(i-1));
				} catch (IndexOutOfBoundsException e) {
					throw new IndexOutOfBoundsException("Attempted to access line " + i + "/" + allLines.size() + " for file " + file.getName());
				}
			}
		}
		
		return new RawCodeSection(startLine, returnedLines);
	}
	
	public static File getTestFile(String relativePathToFile) throws IOException, URISyntaxException {
		String relativePathWithProperPrefix = appendProperTestFilePrefix(relativePathToFile);
		URI absoluteFilePath = getFileResource(relativePathWithProperPrefix);
		return new File(absoluteFilePath);
	}
	
	private static String appendProperTestFilePrefix(String input) {
		String prefix = "testfiles";
		if (!input.startsWith("/"))
			prefix += "/";
		return prefix + input;
	}
	
	private static URI getFileResource(String pathToResource) throws IOException, URISyntaxException {
		URL resource = TestUtils.class.getClassLoader().getResource(pathToResource);
		if (resource == null)
			throw new IOException("TestUtils.getTestFile() - No resource found at path " + pathToResource);
		return new URI(resource.toString());
	}
	
	
}
