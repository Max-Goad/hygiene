package com.maxgoad.hygiene.main;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.hamcrest.core.StringContains;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import com.maxgoad.hygiene.error.FileAccessException;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.main.Hygiene;

public class HygieneTests {

	private Hygiene testApplication;
	
	@Rule 
	public ExpectedException thrown = ExpectedException.none();
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	@Before
	public void setUp() throws Exception {
		testApplication = new Hygiene();
	}

	@After
	public void tearDown() throws Exception {
		testApplication = null;
	}

	@Test
	public void testProduceResultsOnSingleFileAndSingleGuideline() {
		//Create temporary file
		File tempFile = null;
		try {
			tempFile = tempFolder.newFile("temp.java");
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		//Run produceReport
		try {
			testApplication.produceReport(tempFile, GuidelineType.TEST);
		} catch (IllegalArgumentException | FileAccessException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testProduceResultsOnSingleFileAndMultipleGuidelines() {
		//Create temporary file
		File tempFile = null;
		try {
			tempFile = tempFolder.newFile("temp.java");
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		//Run produceReport
		try {
			testApplication.produceReport(tempFile, GuidelineType.TEST, GuidelineType.TEST2);
		} catch (IllegalArgumentException | FileAccessException e) {
			fail(e.getMessage());
		}
		
		//TODO: Replace GuidelineTypes above with more suitable test types
	}
	
	@Test
	public void testProduceResultsOnDirectoryAndSingleGuideline() {
		//Create multiple temp files
		try {
			tempFolder.newFile("temp01.java");
			tempFolder.newFile("temp02.java");
			tempFolder.newFile("temp03.java");
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		//Use temp folder root as input
		File tempDirectory = tempFolder.getRoot();
		
		//Run produceReport
		try {
			testApplication.produceReport(tempDirectory, GuidelineType.TEST);
		} catch (IllegalArgumentException | FileAccessException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testProduceResultsOnDirectoryAndMultipleGuidelines() {
		//Create multiple temp files
		try {
			tempFolder.newFile("temp01.java");
			tempFolder.newFile("temp02.java");
			tempFolder.newFile("temp03.java");
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		//Use temp folder root as input
		File tempDirectory = tempFolder.getRoot();
		
		//Run produceReport
		try {
			testApplication.produceReport(tempDirectory, GuidelineType.TEST, GuidelineType.TEST2);
		} catch (IllegalArgumentException | FileAccessException e) {
			fail(e.getMessage());
		}
		
		//TODO: Replace GuidelineTypes above with more suitable test types
	}
	
	@Test
	public void testProduceResultsWhenIncorrectFileGiven() {
		//Create temporary file
		File tempFile = null;
		try {
			tempFile = tempFolder.newFile("temp.txt");
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		//Set up exception expectations
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage(StringContains.containsString("JavaFileFilter.returnIfJavaFile"));
		
		//Run produceReport
		try {
			testApplication.produceReport(tempFile, GuidelineType.TEST);
		} catch (FileAccessException e) {
			fail(e.getMessage());
		}
	}

}
