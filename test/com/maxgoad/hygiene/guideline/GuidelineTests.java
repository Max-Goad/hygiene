package com.maxgoad.hygiene.guideline;

import static org.junit.Assert.*;

import org.hamcrest.core.StringContains;
import org.junit.*;
import org.junit.rules.ExpectedException;

import com.maxgoad.hygiene.guideline.behavior.GuidelineBehavior;
import com.maxgoad.hygiene.guideline.behavior.TestBehavior;
import com.maxgoad.hygiene.guideline.behavior.comments.*;
import com.maxgoad.hygiene.guideline.behavior.error.*;
import com.maxgoad.hygiene.guideline.behavior.formatting.*;
import com.maxgoad.hygiene.guideline.behavior.functions.*;
import com.maxgoad.hygiene.guideline.behavior.names.*;
import com.maxgoad.hygiene.guideline.behavior.structure.*;

public class GuidelineTests {

	private final GuidelineType testType = GuidelineType.TEST;
	
	@Rule 
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void whenCorrectTypeIsGivenRuleIsCreated() {
		Guideline testGuideline = Guideline.CreateNewGuideline(testType);
		
		assertFalse(testGuideline == null);
	}
	
	@Test
	public void whenIncorrectTypeIsGivenRuleIsNotCreated() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage(StringContains.containsString("GuidelineBehavior.createBehavior"));
		
		Guideline.CreateNewGuideline(GuidelineType.ERROR);
		
		fail("IllegalArgumentException should have been thrown!");
	}
	
	@Test
	public void testAllGuidelineTypesWorkAsIntended() {
		GuidelineType[] types = GuidelineType.values();
		Class<?>[] expectedReturnClasses = new Class[types.length];
		
		//Fill out actual types to be tested, interleaved with what we expect to get back from them
		//IMPORTANT: These must be in the same order as the enum in GuidelineType.java!
		int counter = 0;
		
		expectedReturnClasses[counter++] = TestBehavior.class;
		expectedReturnClasses[counter++] = TestBehavior.class;
		expectedReturnClasses[counter++] = IllegalArgumentException.class;
		
		expectedReturnClasses[counter++] = AvoidClosingBraceMarkersBehavior.class;
		expectedReturnClasses[counter++] = AvoidHTMLCommentsBehavior.class;
		expectedReturnClasses[counter++] = AvoidPositionMarkersBehavior.class;
		
		expectedReturnClasses[counter++] = AvoidNullErrorMessagesBehavior.class;
		expectedReturnClasses[counter++] = AvoidPassingNullBehavior.class;
		expectedReturnClasses[counter++] = AvoidReturningNullBehavior.class;
		expectedReturnClasses[counter++] = ExtractErrorHandlingBehavior.class;
		expectedReturnClasses[counter++] = ExtractTryBodyBehavior.class;

		expectedReturnClasses[counter++] = KeepFileHorizontalLinesSmallBehavior.class;
		expectedReturnClasses[counter++] = KeepFileVerticalLinesSmallBehavior.class;
		
		expectedReturnClasses[counter++] = AvoidFlagArgumentsBehavior.class;
		expectedReturnClasses[counter++] = AvoidManyArgumentsBehavior.class;
		expectedReturnClasses[counter++] = AvoidSwitchStatementsBehavior.class;
		expectedReturnClasses[counter++] = KeepBlocksSmallBehavior.class;
		expectedReturnClasses[counter++] = KeepCommandQuerySeparateBehavior.class;
		expectedReturnClasses[counter++] = KeepFunctionsSmallBehavior.class;

		expectedReturnClasses[counter++] = AvoidOverloadedConstructorsBehavior.class;
		expectedReturnClasses[counter++] = AvoidPrefixesBehavior.class;
		expectedReturnClasses[counter++] = AvoidTypeEncodingsBehavior.class;
		
		expectedReturnClasses[counter++] = AvoidTrainWrecksBehavior.class;
		
		for (int i = 0; i < types.length; i++) {
			try {
				GuidelineBehavior actualReturnClass = GuidelineBehavior.createBehavior(types[i]);
				assertEquals(expectedReturnClasses[i], actualReturnClass.getClass());
			} catch (IllegalArgumentException | UnsupportedOperationException e) {
				assertEquals(expectedReturnClasses[i], e.getClass());
			}
		}
	}

}
