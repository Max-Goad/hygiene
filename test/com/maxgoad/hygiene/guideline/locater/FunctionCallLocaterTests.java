package com.maxgoad.hygiene.guideline.locater;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class FunctionCallLocaterTests extends LocaterTests {

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new FunctionCallLocater();
	}

	@Override
	protected String getTestFileName() {
		return "locater/FunctionCallTest.test";
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(LinePair.oneLinePair(4));
		lineNumbers.add(LinePair.oneLinePair(5));
		lineNumbers.add(LinePair.oneLinePair(6));
		lineNumbers.add(LinePair.oneLinePair(8));
		lineNumbers.add(LinePair.oneLinePair(9));
		lineNumbers.add(LinePair.oneLinePair(10));
		lineNumbers.add(LinePair.oneLinePair(12));
		lineNumbers.add(LinePair.oneLinePair(13));
		lineNumbers.add(LinePair.oneLinePair(14));
		lineNumbers.add(LinePair.oneLinePair(16));
		lineNumbers.add(LinePair.oneLinePair(17));
		lineNumbers.add(LinePair.oneLinePair(18));
		return lineNumbers;
	}

}
