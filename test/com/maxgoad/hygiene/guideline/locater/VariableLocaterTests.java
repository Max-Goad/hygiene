package com.maxgoad.hygiene.guideline.locater;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class VariableLocaterTests extends LocaterTests {
	
	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new VariableLocater();
	}

	@Override
	protected String getTestFileName() {
		return "locater/VariableTest.test";
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(LinePair.oneLinePair(5));
		lineNumbers.add(LinePair.oneLinePair(6));
		lineNumbers.add(LinePair.oneLinePair(7));
		lineNumbers.add(LinePair.oneLinePair(8));
		lineNumbers.add(LinePair.oneLinePair(9));
		lineNumbers.add(LinePair.oneLinePair(10));
		lineNumbers.add(LinePair.oneLinePair(11));
		lineNumbers.add(LinePair.oneLinePair(12));
		lineNumbers.add(LinePair.oneLinePair(14));
		lineNumbers.add(LinePair.oneLinePair(15));
		lineNumbers.add(LinePair.oneLinePair(20));
		lineNumbers.add(LinePair.oneLinePair(21));
		lineNumbers.add(LinePair.oneLinePair(23));
		lineNumbers.add(LinePair.oneLinePair(24));
		lineNumbers.add(LinePair.oneLinePair(30));
		lineNumbers.add(LinePair.oneLinePair(31));
		lineNumbers.add(LinePair.oneLinePair(38));
		lineNumbers.add(LinePair.oneLinePair(39));
		lineNumbers.add(LinePair.oneLinePair(41));
		lineNumbers.add(LinePair.oneLinePair(42));
		lineNumbers.add(LinePair.oneLinePair(43));
		lineNumbers.add(LinePair.oneLinePair(44));
		lineNumbers.add(LinePair.oneLinePair(45));
		lineNumbers.add(LinePair.oneLinePair(47));
		lineNumbers.add(LinePair.oneLinePair(48));
		return lineNumbers;
	}
}
