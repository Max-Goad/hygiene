package com.maxgoad.hygiene.guideline.locater;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class BlockStatementLocaterTests extends LocaterTests {

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new BlockStatementLocater();
	}

	@Override
	protected String getTestFileName() {
		return "locater/BlockStatementTest.test";
	}
	
	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(new LinePair(5,6));
		lineNumbers.add(new LinePair(8,9));
		lineNumbers.add(new LinePair(11,12));
		lineNumbers.add(new LinePair(14,15));
		lineNumbers.add(new LinePair(17,18));
		lineNumbers.add(new LinePair(20,21));
		lineNumbers.add(new LinePair(23,24));
		lineNumbers.add(new LinePair(26,27));
		lineNumbers.add(new LinePair(29,30));
		lineNumbers.add(new LinePair(32,33));
		lineNumbers.add(new LinePair(35,36));
		lineNumbers.add(new LinePair(38,39));
		lineNumbers.add(LinePair.oneLinePair(41));
		lineNumbers.add(LinePair.oneLinePair(43));
		lineNumbers.add(LinePair.oneLinePair(45));
		lineNumbers.add(LinePair.oneLinePair(47));
		lineNumbers.add(LinePair.oneLinePair(49));
		return lineNumbers;
	}

}
