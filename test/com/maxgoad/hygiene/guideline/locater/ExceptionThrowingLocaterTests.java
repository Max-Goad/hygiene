package com.maxgoad.hygiene.guideline.locater;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import com.maxgoad.hygiene.guideline.SimpleTest;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.utils.TestUtils;

public abstract class ExceptionThrowingLocaterTests extends SimpleTest<RawCodeSection> {

	private JavaOccurrenceLocater testLocater;
	
	protected abstract JavaOccurrenceLocater getLocater();
	protected abstract Set<LinePair> getExpectedLines();

	@Override
	protected void acquireAdditionalValues() {
		if (testLocater == null)
			testLocater = getLocater();
	}
	
	@Override
	protected void populateExpectedReturn(File f) {
		for (LinePair each : getExpectedLines()) {
			try {
				expectedReturn.add(TestUtils.getCodeSection(f, each.start, each.end));
			} catch (IOException e) { /* Nothing */ }
		}
	}
	
	@Override
	protected void populateActualReturn(File f) throws IOException {
		actualReturn = testLocater.findOccurrences(f);
	}
}
