package com.maxgoad.hygiene.guideline.locater;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.guideline.locater.RegexLocater;
import com.maxgoad.hygiene.utils.TestUtils;

public class RegexLocaterTests {
	
	private RegexLocater testLocater;
	private File testFile;
	private Set<RawCodeSection> expectedReturn;
	private Set<RawCodeSection> actualReturn;
	
	@Rule 
	public ExpectedException thrown = ExpectedException.none();
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	@Before
	public void setup() throws Exception {
		testLocater = null;
		testFile = null;
		expectedReturn = new HashSet<>();
		actualReturn = null;
	}
	
	@Test
	public void testEmptyFile() {
		testLocater = new RegexLocater("");
		
		try {
			testFile = TestUtils.getTestFile("locater/Empty.test");
			actualReturn = testLocater.findOccurrences(testFile);
		} catch (IOException | URISyntaxException e) {
			fail(e.getMessage());
		}
		
		assertTrue(actualReturn.isEmpty());
	}
	
	@Test
	public void testNoOccurrencesInFile() {
		testLocater = new RegexLocater("final");
		
		try {
			testFile = TestUtils.getTestFile("locater/RegexTest.test");
			actualReturn = testLocater.findOccurrences(testFile);
		} catch (IOException | URISyntaxException e) {
			fail(e.getMessage());
		}
		
		assertTrue(actualReturn.isEmpty());
	}

	@Test
	public void testSingleOccurrenceInFile() {
		testLocater = new RegexLocater("[public|protected|private] static [void|int|String]");
		
		try {
			testFile = TestUtils.getTestFile("locater/RegexTest.test");
			
			expectedReturn.add(TestUtils.getCodeSection(testFile, 15, 15));
			
			actualReturn = testLocater.findOccurrences(testFile);
			
		} catch (IOException | URISyntaxException e) {
			fail(e.getMessage());
		}
		
		TestUtils.assertEqualSets(expectedReturn, actualReturn);
	}

	@Test
	public void testMultipleOccurrencesInFile() {
		testLocater = new RegexLocater("WhatIsTheMeaningOfLife\\(\\)");
		
		try {
			testFile = TestUtils.getTestFile("locater/RegexTest.test");
			
			expectedReturn.add(TestUtils.getCodeSection(testFile, 15, 15));
			expectedReturn.add(TestUtils.getCodeSection(testFile, 40, 40));
			
			actualReturn = testLocater.findOccurrences(testFile);
			
		} catch (IOException | URISyntaxException e) {
			fail(e.getMessage());
		}

		TestUtils.assertEqualSets(expectedReturn, actualReturn);
	}
}
