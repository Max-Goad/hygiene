package com.maxgoad.hygiene.guideline.locater;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class TryBlockLocaterTests extends LocaterTests {

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new TryBlockLocater();
	}

	@Override
	protected String getTestFileName() {
		return "locater/ErrorHandlingTest.test";
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(new LinePair(6,8));
		lineNumbers.add(new LinePair(13,15));
		return lineNumbers;
	}

}
