package com.maxgoad.hygiene.guideline.locater;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class CommentLocaterTests extends ExceptionThrowingLocaterTests {

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new CommentLocater();
	}

	@Override
	protected String getTestFileName() {
		return "locater/CommentTest.test";
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(LinePair.oneLinePair(7));
		lineNumbers.add(LinePair.oneLinePair(16));
		lineNumbers.add(new LinePair(20,22));
		lineNumbers.add(LinePair.oneLinePair(23));
		lineNumbers.add(LinePair.oneLinePair(25));
		return lineNumbers;
	}
	
	@Override
	protected boolean isValidException(Exception e) {
		System.out.println(getLocater().getClass().getName() + " poops you");
		return false;
	}
}
