package com.maxgoad.hygiene.guideline.locater;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class FunctionLocaterTests extends LocaterTests {

	@Override
	protected JavaOccurrenceLocater getLocater() {
		return new FunctionLocater();
	}

	@Override
	protected String getTestFileName() {
		return "locater/FunctionTest.test";
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(new LinePair(5,6));
		lineNumbers.add(new LinePair(8,9));
		lineNumbers.add(new LinePair(11,12));
		lineNumbers.add(new LinePair(14,24));
		lineNumbers.add(LinePair.oneLinePair(30));
		return lineNumbers;
	}

}
