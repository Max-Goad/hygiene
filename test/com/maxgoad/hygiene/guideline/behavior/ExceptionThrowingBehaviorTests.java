package com.maxgoad.hygiene.guideline.behavior;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineMatch;
import com.maxgoad.hygiene.guideline.SimpleTest;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;
import com.maxgoad.hygiene.guideline.codechunk.RawCodeSection;
import com.maxgoad.hygiene.utils.TestUtils;

public abstract class ExceptionThrowingBehaviorTests extends SimpleTest<GuidelineMatch> {
	
	private Guideline guideline;
	
	protected abstract Guideline getGuideline();
	protected abstract Set<LinePair> getExpectedLines();
	
	@Override
	protected void acquireAdditionalValues() {
		if (guideline == null)
			guideline = getGuideline();
	}
	
	@Override
	protected void populateExpectedReturn(File f) {
		for (LinePair each : getExpectedLines()) {
			RawCodeSection codeSection;
			try {
				codeSection = TestUtils.getCodeSection(f, each.start, each.end);
				expectedReturn.add(new GuidelineMatch(guideline, f, codeSection));
			} catch (IOException e) { /* Nothing */ }
		}
	}
	
	@Override
	protected void populateActualReturn(File f) throws IOException {
		actualReturn = guideline.findPotentialProblems(f);
	}
	
	@Override
	protected boolean isValidException(Exception e) {
		return false;
	}
}
