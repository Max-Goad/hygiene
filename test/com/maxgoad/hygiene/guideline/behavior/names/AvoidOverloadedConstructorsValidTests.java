package com.maxgoad.hygiene.guideline.behavior.names;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.ExceptionThrowingBehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;
import com.maxgoad.hygiene.guideline.locater.ConstructorLocater;

public class AvoidOverloadedConstructorsValidTests extends ExceptionThrowingBehaviorTests {

	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.AVOID_OVERLOADED_CONSTRUCTORS);
	}

	@Override
	protected String getTestFileName() {
		return "behavior/AvoidOverloadedConstructorsTest.test";
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		return Sets.newSet();
	}
	
	@Override
	protected boolean isValidException(Exception e) {
		return e.getMessage().startsWith(ConstructorLocater.MISSING_CLASS_HEADER_ERROR_MESSAGE);
	}

}
