package com.maxgoad.hygiene.guideline.behavior.names;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class AvoidTypeEncodingsTests extends BehaviorTests {

	@Override
	protected String getTestFileName() {
		return "behavior/AvoidTypeEncodingsTest.test";
	}

	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.AVOID_TYPE_ENCODINGS);
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(LinePair.oneLinePair(5));
		lineNumbers.add(LinePair.oneLinePair(6));
		lineNumbers.add(LinePair.oneLinePair(7));
		lineNumbers.add(LinePair.oneLinePair(8));
		lineNumbers.add(LinePair.oneLinePair(9));
		lineNumbers.add(LinePair.oneLinePair(10));
		lineNumbers.add(LinePair.oneLinePair(11));
		lineNumbers.add(LinePair.oneLinePair(12));
		lineNumbers.add(LinePair.oneLinePair(13));
		lineNumbers.add(LinePair.oneLinePair(14));
		lineNumbers.add(LinePair.oneLinePair(15));
		lineNumbers.add(LinePair.oneLinePair(16));
		lineNumbers.add(LinePair.oneLinePair(17));
		lineNumbers.add(LinePair.oneLinePair(18));
		lineNumbers.add(LinePair.oneLinePair(19));
		lineNumbers.add(LinePair.oneLinePair(20));
		lineNumbers.add(LinePair.oneLinePair(21));
		lineNumbers.add(LinePair.oneLinePair(23));
		lineNumbers.add(LinePair.oneLinePair(26));
		lineNumbers.add(LinePair.oneLinePair(27));
		lineNumbers.add(LinePair.oneLinePair(28));
		return lineNumbers;
	}
}
