package com.maxgoad.hygiene.guideline.behavior.names;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.ExceptionThrowingBehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;
import com.maxgoad.hygiene.guideline.locater.ConstructorLocater;

public class AvoidOverloadedConstructorsErrorTests extends ExceptionThrowingBehaviorTests {

	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.AVOID_OVERLOADED_CONSTRUCTORS);
	}

	@Override
	protected String getTestFileName() {
		return "locater/ConstructorTest.test";
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(new LinePair(5,6));
		lineNumbers.add(new LinePair(8,9));
		lineNumbers.add(new LinePair(11,12));
		lineNumbers.add(new LinePair(14,15));
		lineNumbers.add(new LinePair(17,20));
		lineNumbers.add(new LinePair(22,23));
		lineNumbers.add(new LinePair(25,26));
		return lineNumbers;
	}
	
	@Override
	protected boolean isValidException(Exception e) {
		return e.getMessage().startsWith(ConstructorLocater.MISSING_CLASS_HEADER_ERROR_MESSAGE);
	}

}
