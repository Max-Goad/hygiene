package com.maxgoad.hygiene.guideline.behavior.formatting;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class KeepFileHorizontalLinesSmallTests extends BehaviorTests {

	@Override
	protected String getTestFileName() {
		return "behavior/KeepFileHorizontalLinesSmallTest.test";
	}
	
	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.KEEP_FILE_HORIZONAL_LINES_SMALL);
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		return Sets.newSet(LinePair.oneLinePair(6));
	}
}
