package com.maxgoad.hygiene.guideline.behavior.formatting;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class KeepFileVerticalLinesSmallTests extends BehaviorTests {

	@Override
	protected String getTestFileName() {
		return "behavior/KeepFileVerticalLinesSmallTest.test";
	}
	
	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.KEEP_FILE_VERTICAL_LINES_SMALL);
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(LinePair.oneLinePair(3));
		lineNumbers.add(LinePair.oneLinePair(8));
		lineNumbers.add(LinePair.oneLinePair(13));
		lineNumbers.add(LinePair.oneLinePair(17));
		lineNumbers.add(LinePair.oneLinePair(21));
		return lineNumbers;
	}
}
