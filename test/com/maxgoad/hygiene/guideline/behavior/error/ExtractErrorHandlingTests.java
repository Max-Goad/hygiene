package com.maxgoad.hygiene.guideline.behavior.error;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class ExtractErrorHandlingTests extends BehaviorTests {

	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.EXTRACT_ERROR_HANDLING);
	}

	@Override
	protected String getTestFileName() {
		return "behavior/ExtractErrorHandlingTest.test";
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(new LinePair(45,52));
		lineNumbers.add(new LinePair(54,61));
		lineNumbers.add(new LinePair(63,75));
		return lineNumbers;
	}

}
