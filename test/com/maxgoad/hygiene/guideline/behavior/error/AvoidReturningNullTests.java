package com.maxgoad.hygiene.guideline.behavior.error;

import java.util.HashSet;
import java.util.Set;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class AvoidReturningNullTests extends BehaviorTests {

	@Override
	protected String getTestFileName() {
		return "behavior/NullTest.test";
	}
	
	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.AVOID_RETURNING_NULL);
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = new HashSet<>();
		lineNumbers.add(LinePair.oneLinePair(5));
		lineNumbers.add(LinePair.oneLinePair(9));
		lineNumbers.add(LinePair.oneLinePair(21));
		return lineNumbers;
	}

}
