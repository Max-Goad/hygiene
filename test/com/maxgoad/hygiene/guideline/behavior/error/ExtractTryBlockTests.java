package com.maxgoad.hygiene.guideline.behavior.error;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class ExtractTryBlockTests extends BehaviorTests {

	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.EXTRACT_TRY_BODY);
	}

	@Override
	protected String getTestFileName() {
		return "behavior/ExtractTryBodyTest.test";
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(new LinePair(12,15));
		lineNumbers.add(new LinePair(36,39));
		lineNumbers.add(new LinePair(43,46));
		return lineNumbers;
	}

}
