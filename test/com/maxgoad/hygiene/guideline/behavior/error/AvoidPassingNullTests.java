package com.maxgoad.hygiene.guideline.behavior.error;

import java.util.HashSet;
import java.util.Set;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class AvoidPassingNullTests extends BehaviorTests {

	@Override
	protected String getTestFileName() {
		return "behavior/NullTest.test";
	}
	
	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.AVOID_PASSING_NULL);
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = new HashSet<>();
		lineNumbers.add(LinePair.oneLinePair(23));
		lineNumbers.add(LinePair.oneLinePair(24));
		lineNumbers.add(LinePair.oneLinePair(25));
		lineNumbers.add(LinePair.oneLinePair(26));
		lineNumbers.add(LinePair.oneLinePair(27));
		lineNumbers.add(LinePair.oneLinePair(28));
		return lineNumbers;
	}

}
