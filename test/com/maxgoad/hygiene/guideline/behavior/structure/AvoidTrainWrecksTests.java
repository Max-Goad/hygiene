package com.maxgoad.hygiene.guideline.behavior.structure;

import java.util.HashSet;
import java.util.Set;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class AvoidTrainWrecksTests extends BehaviorTests {

	@Override
	protected String getTestFileName() {
		return "behavior/AvoidTrainWrecksTest.test";
	}

	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.AVOID_TRAIN_WRECKS);
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = new HashSet<>();
		lineNumbers.add(LinePair.oneLinePair(35));
		lineNumbers.add(LinePair.oneLinePair(36));
		lineNumbers.add(LinePair.oneLinePair(40));
		return lineNumbers;
	}

}
