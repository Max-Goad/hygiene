package com.maxgoad.hygiene.guideline.behavior.functions;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class AvoidSwitchStatementsValidTests extends BehaviorTests {

	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.AVOID_SWITCH_STATEMENTS);
	}

	@Override
	protected String getTestFileName() {
		return "behavior/AvoidSwitchStatementsValidTest.test";
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		return lineNumbers;
	}

}
