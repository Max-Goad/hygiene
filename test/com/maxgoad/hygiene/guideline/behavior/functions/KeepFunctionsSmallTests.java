package com.maxgoad.hygiene.guideline.behavior.functions;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class KeepFunctionsSmallTests extends BehaviorTests {

	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.KEEP_FUNCTIONS_SMALL);
	}

	@Override
	protected String getTestFileName() {
		return "behavior/KeepFunctionsSmallTest.test";
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(new LinePair(34, 46));
		lineNumbers.add(new LinePair(48, 149));
		return lineNumbers;
	}

}
