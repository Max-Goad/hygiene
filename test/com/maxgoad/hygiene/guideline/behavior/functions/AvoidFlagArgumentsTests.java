package com.maxgoad.hygiene.guideline.behavior.functions;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class AvoidFlagArgumentsTests extends BehaviorTests {

	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.AVOID_FLAG_ARGUMENTS);
	}

	@Override
	protected String getTestFileName() {
		return "behavior/AvoidFlagArgumentsTest.test";
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(new LinePair(5,6));
		lineNumbers.add(new LinePair(8,9));
		lineNumbers.add(new LinePair(11,12));
		return lineNumbers;
	}

}
