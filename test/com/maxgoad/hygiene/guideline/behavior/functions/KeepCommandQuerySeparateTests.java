package com.maxgoad.hygiene.guideline.behavior.functions;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class KeepCommandQuerySeparateTests extends BehaviorTests {

	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.KEEP_COMMAND_QUERY_SEPARATE);
	}

	@Override
	protected String getTestFileName() {
		return "behavior/KeepCommandQuerySeparateTest.test";
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(new LinePair(14,15));
		lineNumbers.add(new LinePair(26,27));
		return lineNumbers;
	}

}
