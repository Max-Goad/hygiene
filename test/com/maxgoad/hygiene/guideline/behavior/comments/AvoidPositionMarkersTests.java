package com.maxgoad.hygiene.guideline.behavior.comments;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class AvoidPositionMarkersTests extends BehaviorTests {
	
	@Override
	protected String getTestFileName() {
		return "behavior/AvoidPositionMarkersTest.test";
	}
	
	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.AVOID_POSITION_MARKERS);
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = Sets.newSet();
		lineNumbers.add(LinePair.oneLinePair(5));
		lineNumbers.add(LinePair.oneLinePair(7));
		lineNumbers.add(LinePair.oneLinePair(15));
		lineNumbers.add(LinePair.oneLinePair(17));
		lineNumbers.add(LinePair.oneLinePair(19));
		lineNumbers.add(LinePair.oneLinePair(21));
		lineNumbers.add(LinePair.oneLinePair(23));
		lineNumbers.add(LinePair.oneLinePair(25));
		lineNumbers.add(LinePair.oneLinePair(27));
		lineNumbers.add(LinePair.oneLinePair(29));
		lineNumbers.add(LinePair.oneLinePair(31));
		lineNumbers.add(LinePair.oneLinePair(33));
		lineNumbers.add(LinePair.oneLinePair(35));
		lineNumbers.add(LinePair.oneLinePair(37));
		lineNumbers.add(LinePair.oneLinePair(39));
		lineNumbers.add(LinePair.oneLinePair(41));
		lineNumbers.add(LinePair.oneLinePair(43));
		lineNumbers.add(LinePair.oneLinePair(45));
		lineNumbers.add(LinePair.oneLinePair(47));
		lineNumbers.add(LinePair.oneLinePair(49));
		lineNumbers.add(LinePair.oneLinePair(51));
		lineNumbers.add(LinePair.oneLinePair(53));
		lineNumbers.add(new LinePair(55,57));
		lineNumbers.add(new LinePair(59,63));
		return lineNumbers;
	}
}
