package com.maxgoad.hygiene.guideline.behavior.comments;

import java.util.HashSet;
import java.util.Set;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class AvoidHTMLCommentsTests extends BehaviorTests {

	@Override
	protected String getTestFileName() {
		return "behavior/AvoidHTMLCommentsTest.test";
	}
	
	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.AVOID_HTML_COMMENTS);
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		Set<LinePair> lineNumbers = new HashSet<>();
		lineNumbers.add(LinePair.oneLinePair(7));
		lineNumbers.add(LinePair.oneLinePair(16));
		lineNumbers.add(new LinePair(20, 22));
		lineNumbers.add(LinePair.oneLinePair(23));
		return lineNumbers;
	}
}
