package com.maxgoad.hygiene.guideline.behavior.comments;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

import com.maxgoad.hygiene.guideline.Guideline;
import com.maxgoad.hygiene.guideline.GuidelineType;
import com.maxgoad.hygiene.guideline.behavior.BehaviorTests;
import com.maxgoad.hygiene.guideline.codechunk.LinePair;

public class AvoidClosingBraceMarkersTests extends BehaviorTests {

	@Override
	protected String getTestFileName() {
		return "behavior/AvoidClosingBraceMarkersTest.test";
	}

	@Override
	protected Guideline getGuideline() {
		return Guideline.CreateNewGuideline(GuidelineType.AVOID_CLOSING_BRACE_MARKERS);
	}

	@Override
	protected Set<LinePair> getExpectedLines() {
		return Sets.newSet(LinePair.oneLinePair(14), LinePair.oneLinePair(20));
	}
}
