package com.maxgoad.hygiene.guideline;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.maxgoad.hygiene.utils.TestUtils;

public abstract class SimpleTest<T> {

	protected final String emptyFileLocation = "locater/Empty.test";

	protected String testFileName;
	protected Set<T> expectedReturn;
	protected Set<T> actualReturn;

	protected abstract String getTestFileName();
	protected abstract void acquireAdditionalValues();
	protected abstract void populateExpectedReturn(File f);
	protected abstract void populateActualReturn(File f) throws IOException;
	protected abstract boolean isValidException(Exception e);
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	@Before
	public void setup() throws Exception {
		if (testFileName == null)
			testFileName = getTestFileName();
		
		expectedReturn = new HashSet<T>();
		actualReturn = null;
		acquireAdditionalValues();
	}
	
	@Test
	public void testEmptyFile() {
		try {
			File emptyFile = TestUtils.getTestFile(emptyFileLocation);
			populateActualReturn(emptyFile);
		} catch (Exception e) {
			if (isValidException(e)) {
				actualReturn = new HashSet<>();
			}
			else {
				fail(e.getClass().getSimpleName() + " - " + e.getMessage());
			}
		}
		
		assertTrue(actualReturn.isEmpty());
	}
	
	@Test
	public void testUsingTestFile() {
		try {
			File testFile = TestUtils.getTestFile(testFileName);
			populateExpectedReturn(testFile);
			populateActualReturn(testFile);
		} catch (IOException | IndexOutOfBoundsException | URISyntaxException e) {
			fail(e.getClass().getSimpleName() + " - " + e.getMessage());
		}
		
		
		//TODO: Remove before completion of tests
//		System.out.println(expectedReturn.size());
//		System.out.println(expectedReturn);
//		System.out.println(actualReturn.size());
//		System.out.println(actualReturn);
//		System.out.println();
		
		
		TestUtils.assertEqualSets(expectedReturn, actualReturn);
	}
	

}
