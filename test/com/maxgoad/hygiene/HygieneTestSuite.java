package com.maxgoad.hygiene;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.maxgoad.hygiene.analysis.FileAnalyzerTests;
import com.maxgoad.hygiene.analysis.JavaFileFilterTests;
import com.maxgoad.hygiene.guideline.GuidelineTests;
import com.maxgoad.hygiene.guideline.behavior.comments.*;
import com.maxgoad.hygiene.guideline.behavior.error.*;
import com.maxgoad.hygiene.guideline.behavior.formatting.*;
import com.maxgoad.hygiene.guideline.behavior.functions.*;
import com.maxgoad.hygiene.guideline.behavior.names.*;
import com.maxgoad.hygiene.guideline.behavior.structure.*;
import com.maxgoad.hygiene.guideline.locater.*;
import com.maxgoad.hygiene.main.HygieneTests;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	
	//analysis
	FileAnalyzerTests.class,
	JavaFileFilterTests.class,
	
	//guideline
	GuidelineTests.class,
	///locater
	BlockStatementLocaterTests.class,
	CommentLocaterTests.class,
	ConstructorLocaterTests.class,
	FunctionCallLocaterTests.class,
	FunctionLocaterTests.class,
	RegexLocaterTests.class,
	TryBlockLocaterTests.class,
	VariableLocaterTests.class,
	///behavior
	////comments
	AvoidClosingBraceMarkersTests.class,
	AvoidHTMLCommentsTests.class,
	AvoidPositionMarkersTests.class,
	////error
	AvoidNullErrorMessageTests.class,
	AvoidPassingNullTests.class,
	AvoidReturningNullTests.class,
	ExtractErrorHandlingTests.class,
	ExtractTryBlockTests.class,
	////formatting
	KeepFileHorizontalLinesSmallTests.class,
	KeepFileVerticalLinesSmallTests.class,
	////functions
	AvoidFlagArgumentsTests.class,
	AvoidManyArgumentsTests.class,
	AvoidSwitchStatementsValidTests.class,
	AvoidSwitchStatementsErrorTests.class,
	KeepBlocksSmallTests.class,
	KeepCommandQuerySeparateTests.class,
	KeepFunctionsSmallTests.class,
	////names
	AvoidOverloadedConstructorsValidTests.class,
	AvoidOverloadedConstructorsErrorTests.class,
	AvoidPrefixesTests.class,
	AvoidTypeEncodingsTests.class,
	////structure
	AvoidTrainWrecksTests.class,
	
	//main
	HygieneTests.class
	
})

public class HygieneTestSuite {
}
